<?php 
require_once 'global_define.php';
defined('YII_DEBUG') or define('YII_DEBUG',true);
 
// including Yii
// change the following paths if necessary
$yii=dirname(__FILE__).'/framework/yii.php';
 
// we'll use a separate config file
$configFile=dirname(__FILE__).'/protected/config/cron.php';
require_once($yii); 
// creating and running console application
Yii::createConsoleApplication($configFile)->run();

?>