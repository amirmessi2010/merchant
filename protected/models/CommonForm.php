<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class CommonForm extends CFormModel
{
	public $file_name;
        public $ref_array;
        public $type;
        /**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array('file_name', 'required'),			
		);
	}
        
        /**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'type' => 'Format Type',
		);
	}
        
        public function mapReference(){
            return $this->ref_array;
        }
        
        public function setMapReference($data_array=array()){
            return $this->ref_array = $data_array;
        }
        
        public function checkFormat($header){
            $header = array_filter($header);
            $map_reference = $this->mapReference();
            
            $diff_array = array_diff($map_reference, $header);
            
            if(empty($diff_array))
                return true; //the format is okie 
            return false;
        }
	
}