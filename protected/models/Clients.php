<?php

/**
 * This is the model class for table "clients".
 *
 * The followings are the available columns in table 'clients':
 * @property integer $client_id
 * @property string $name
 * @property string $currency
 * @property integer $company_id
 * @property string $country
 * @property string $upload_format
 * @property string $courier_id
 * @property string $created
 * @property string $modified
 */
class Clients extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'clients';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, currency, company_id, upload_format, courier_id', 'required'),
			array('company_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>70),
			array('currency', 'length', 'max'=>4),
			array('courier_id', 'length', 'max'=>10),
			array('modified', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('client_id, name, currency, company_id, country, upload_format, courier_id, created, modified', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'company' => array(self::BELONGS_TO, 'Companies', 'company_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'client_id' => 'Client',
			'name' => 'Name',
			'currency' => 'Currency',
			'company_id' => 'Company',
                        'country' => 'Country',
			'upload_format' => 'Upload Format',
			'courier_id' => 'Courier',
			'created' => 'Created',
			'modified' => 'Modified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('currency',$this->currency,true);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('upload_format',$this->upload_format,true);
		$criteria->compare('courier_id',$this->courier_id,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Clients the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        /*
         */
        
        public function getClients($select_fields = array('*')){
            $criteria = new CDbCriteria();
            $criteria->alias = 'c';
            $criteria->select = implode(',', $select_fields);
//            $criteria->join = " JOIN address a ON a.address_id = c.address_id";
            return Clients::model()->findAll($criteria);
        }
        
        public function getClient($client_id){
            return Clients::model()->findByPk($client_id);
        }

        protected function beforeSave(){
        	if($this->isNewRecord)
        		$this->created = new CDbExpression('Now()');
        	$this->modified = new CDbExpression('Now()');
        	return parent::beforeSave();
        }
}
