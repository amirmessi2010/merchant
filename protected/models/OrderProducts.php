<?php

/**
 * This is the model class for table "order_products".
 *
 * The followings are the available columns in table 'order_products':
 * @property string $op_id
 * @property string $order_id
 * @property string $c2i_sku
 * @property string $client_sku
 * @property string $client_transaction_id
 * @property string $unit_price
 * @property integer $qty
 * @property string $description
 * @property integer $po_id
 * @property integer $status_id
 * @property integer $courier_id
 * @property integer $hold
 * @property integer $receipt_id
 * @property string $create_date
 * @property integer $cod_flag
 * @property integer $dest_wh_id
 */
class OrderProducts extends CActiveRecord
{
        public $filter_by;
        public $filter_value;
        public $date_range;
        public $client_order_no;
        public $email;
        public $mobile;
        public $client_name;
        public $merchant_id;
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'order_products';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('order_id, c2i_sku, client_sku, client_transaction_id, unit_price', 'required'),
                        array('client_transaction_id','filter','filter'=>array($this,'changeScientificNumber')),
			array('qty, po_id, status_id, courier_id, hold, receipt_id, cod_flag, dest_wh_id', 'numerical', 'integerOnly'=>true),
			array('order_id', 'length', 'max'=>11),
//			array('c2i_sku', 'length', 'max'=>20),
			array('client_sku, client_transaction_id', 'length', 'max'=>30),
			array('unit_price', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('op_id, order_id, c2i_sku, client_sku, client_transaction_id, unit_price, qty, description, po_id, status_id, courier_id, hold, receipt_id, create_date, cod_flag, dest_wh_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'order' => array(self::BELONGS_TO, 'Orders', 'order_id'),
                    'status' => array(self::BELONGS_TO, 'Status', 'status_id'),
                    'order_statuses' => array(self::HAS_MANY, 'OpStatus', 'op_id'),                    
                    'merchant' => array(self::BELONGS_TO, 'OpMerchants', 'op_id'),
                    'warehouse' => array(self::BELONGS_TO, 'Warehouse', 'dest_wh_id'),
                    
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'op_id' => 'Op',
			'order_id' => 'Order',
			'c2i_sku' => 'C2i Sku',
			'client_sku' => 'Client Sku',
			'client_transaction_id' => 'Client Transaction',
			'unit_price' => 'Unit Price',
			'qty' => 'Qty',
			'description' => 'Description',
			'po_id' => 'Po',
			'status_id' => 'Status',
			'courier_id' => 'Courier',
			'hold' => 'Hold',
			'receipt_id' => 'Receipt',
			'create_date' => 'Create Date',
			'cod_flag' => 'Cod Flag',
			'dest_wh_id' => 'Dest Wh',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('op_id',$this->op_id,true);
		$criteria->compare('order_id',$this->order_id,true);
		$criteria->compare('c2i_sku',$this->c2i_sku,true);
		$criteria->compare('client_sku',$this->client_sku,true);
		$criteria->compare('client_transaction_id',$this->client_transaction_id,true);
		$criteria->compare('unit_price',$this->unit_price,true);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('po_id',$this->po_id);
		$criteria->compare('status_id',$this->status_id);
		$criteria->compare('courier_id',$this->courier_id);
		$criteria->compare('hold',$this->hold);
		$criteria->compare('receipt_id',$this->receipt_id);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('cod_flag',$this->cod_flag);
		$criteria->compare('dest_wh_id',$this->dest_wh_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>array(
                            'pageSize'=>  100,
                        ),
		));
	}
        
        public function overview($status_id,$merchant_id,$date_range,$verified_flag = null)
	{
		
                $map_filter_by = array(
                    ''=>'All',
                    'order_id'=>'t.order_id',
                    'client_order_no'=>'o.client_order_no',
                    'email'=>'email',                    
                    'description'=>'t.description',
                    'client_name' => 'c.name',
                    'purchase_order_id'=>'t.po_id',  
                    'lc_tracking_id' => 'tr.lc_tracking_id',
                    'invoice_number' => 'p.invoice_no',
                    'shipment_no' => 'tr.shipment_no'
                );
                /*$filterby_list = 
                        array(
                            ''=>'All',
                            'order_id'=>'Order Id',
                            'client_order_no'=>'Client Order NO.',
                            'email'=>'t.email',
                            'name'=>'t.first_a',
                            
                            'client_name'=>'Client Name',
                            'product_title'=>'Product Title',
                            'purchase_order_id'=>'Purchase Order Id',
                            'invoice_number'=>'Invoice Number',
                            'lc_tracking_id'=>'Lc Tracking Id',
                        );*/
		$criteria=new CDbCriteria;
                
                
                if(!empty($status_id) || $status_id == 0)
                    $criteria->compare('t.status_id',$status_id);
                
                if(isset($merchant_id) || empty($merchant_id))
                    $criteria->compare ('om.merchant_id', $merchant_id);                
                
                if(isset($verified_flag)){
                    $criteria->compare ('om.verified_flag', 1);
                }
                
                if(!empty($date_range)){
                        $pos = strpos($date_range, '-');

                        if($pos===false){
                            $criteria->addCondition('t.create_date = :create_date', 'AND');
                            $criteria->params += array(':create_date' =>  date('Y-m-d',strtotime($date_range)));                            
                        }                            
                        else{
                            $date_arr = explode('-',$date_range);
                            
                            $criteria->addCondition('t.create_date >= :create_date1 AND o.create_date <= :create_date2', 'AND');
                            $criteria->params += array(':create_date1' =>  date('Y-m-d',strtotime($date_arr[0])),':create_date2' =>  date('Y-m-d',strtotime($date_arr[1] . "+1 days"))); 
                            
                                                         
                        }
                    }
                    
                
               
                
                $criteria->join = 
                        ' JOIN orders as o on t.order_id = o.order_id 
                          JOIN op_merchants as om on om.op_id = t.op_id  
                          JOIN status as s on t.status_id = s.status_id
                        ';        
//                $criteria->with = array('order');
                $sort = new CSort();
                $sort->defaultOrder= array(
                    'order_id'=>true
                  );
                $sort->attributes = array(
                        'order_id'=>array(                                
                                'asc'=>'t.order_id',
                                'desc'=>'t.order_id desc',
                        ),
                        'client'=>array(                                
                                'asc'=>'c.name',
                                'desc'=>'c.name desc',
                        ),
                        'status_name'=>array(   
                                'asc'=>'s.name',
                                'desc'=>'s.name desc',
                        ),
                );
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort' => $sort,
                        'pagination'=>array(
                            'pageSize'=>  100,
                        ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderProducts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getOrderProduct($op_id){
            return self::model()->findByPk($op_id);
        }
        
        public function getOrderProductPO($po_id){
            return self::model()->findByAttributes(array('po_id'=>$po_id));
        }
        
        public function checkOrderProduct($data)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->compare('order_id',isset($data['order_id'])?$data['order_id']:null);
		$criteria->compare('client_transaction_id',isset($data['client_transaction_id'])?  $this->changeScientificNumber($data['client_transaction_id']):null);		

                
		return self::model()->find($criteria);
	}
        
        public function addOrderProduct($data,$client){
            
            $order_product = new OrderProducts;
            
            $op_data = array(
			'order_id' => isset($data['order_id'])?$data['order_id']:null,
			'c2i_sku' => isset($data['c2i_sku'])?$data['c2i_sku']:0, 
                        'client_sku' => isset($data['client_sku'])?$data['client_sku']:0, 
			'client_transaction_id' => isset($data['client_transaction_id'])?$data['client_transaction_id']:null,
			'unit_price' => isset($data['unit_price'])?$data['unit_price']:null,
			'qty' => isset($data['qty'])?$data['qty']:1,
			'description' => isset($data['description'])?$data['description']:null,
			'po_id' => isset($data['po_id'])?$data['po_id']:null,
			'status_id' => isset($data['status_id'])?$data['status_id']:1,
			'courier_id' => isset($data['courier_id'])?$data['courier_id']:null,
			'hold' => isset($data['hold'])?$data['hold']:null,
			'receipt_id' => isset($data['receipt_id'])?$data['receipt_id']:null,
			'cod_flag' => isset($data['cod_flag'])?$data['cod_flag']:null,
		);
            
            
            
            $order_product->setAttributes($op_data); 
            
            $order_product->description = $data['description'];
            
            if(!isset($data['client_sku']) || $data['client_sku'] == 0){
                $order_product->client_sku = $order_product->c2i_sku;
            }
            
            if($client->client_id == AMAZON){
                $order_product->unit_price = floatval($data['unit_price']/$data['qty']);
            }            
            
            if($order_product->validate() && $order_product->save()){
                return array(
                    'status' => true,
                    'data' => $order_product
                );
            }
            else {
                return array(
                    'status' => false,
                    'errors' => $order_product->getErrors()
                );
            }
        }
        
        public function updateOrderProduct($op_id,$data){
            
            $order_product = self::model()->findByPk($op_id);
            
            foreach($data as $key => $val){
                $order_product->$key = $val;
            }
            
            if($order_product->save()){
                return array(
                    'status' => true,
                    'data' => $order_product
                );
            }
            else {
                return array(
                    'status' => false,
                    'errors' => $order_product->getErrors()
                );
            }
        }
        
        public function updateOrderProductByPO($po_id,$data){
            
            $order_product = self::model()->findByAttributes(array('po_id' => $po_id));
            
            $order_product->setAttributes($data);
            
            if($order_product->save()){
                return array(
                    'status' => true,
                    'data' => $order_product
                );
            }
            else {
                return array(
                    'status' => false,
                    'errors' => $order_product->getErrors()
                );
            }
        }
        
        public function getStatus($op_id){
            
        }
        public function getMerchantInformation(){
            $criteria = new CDbCriteria();
            $criteria->select = 'om.merchant_sku as merchant_sku, a.nickname as merchant_name';
            $criteria->join = '
                LEFT JOIN op_merchants as om  on om.op_id = t.op_id
                LEFT JOIN merchants as m on om.merchant_id = m.merchant_id   
                LEFT JOIN address as a on a.address_id = m.address_id
                ';
            
            $criteria->condition = 't.op_id = '.$this->op_id;            
            
            return OrderProducts::model()->find($criteria);
        }
        
        
        public function getOrderMappingProducts(){
            $count=Yii::app()->db->createCommand('
                SELECT 
                    COUNT(*) 
                FROM order_products as op 
                LEFT JOIN op_merchants om ON om.op_id = op.op_id 
                WHERE status_id = 1 and om.merchant_id != 0 ')->queryScalar();
            $sql='
                SELECT                    
                    op.order_id,op.op_id,om.merchant_id,om.merchant_sku,op.description,op.unit_price,op.qty 
                FROM order_products as op 
                LEFT JOIN op_merchants om ON om.op_id = op.op_id 
                WHERE op.status_id = 1 and om.merchant_id != 0 ';
            return new CSqlDataProvider($sql, array(
                'totalItemCount'=>$count,                
                'pagination'=>array(
                    'pageSize'=>10000000,
                ),
            ));
        }
        
        public function getUnmapProducts(){
            $count=Yii::app()->db->createCommand('
                SELECT 
                    COUNT(*) 
                FROM order_products as op 
                LEFT JOIN op_merchants om ON om.op_id = op.op_id 
                WHERE om.merchant_id = 0')->queryScalar();
            $sql='
                SELECT 
                    op.order_id,op.op_id,om.merchant_id,om.merchant_sku,op.description,op.unit_price,op.qty 
                FROM order_products as op 
                LEFT JOIN op_merchants om ON om.op_id = op.op_id 
                WHERE om.merchant_id = 0';
            return new CSqlDataProvider($sql, array(
                'totalItemCount'=>$count,                
                'pagination'=>array(
                    'pageSize'=>10000000,
                ),
            ));
            
        }
        
        public function isScientificStr($str) {
            $str = trim($str);
            if (empty($str)) return FALSE;
            if (strpos($str, '.')) return TRUE;
            if (strpos($str,'+')) return TRUE;

            return FALSE;

        }
        
        public function checkScientificNumber($number){
            $str_arr = array('+','.');
            $str_con_flag = false;
            foreach($str_arr as $str){
                if(strpos($number, $str) !== false){
                    $str_con_flag = true;
                }
            }
            
            return $str_con_flag;
        }
        
        //change the scientificnumber to float
        public function changeScientificNumber($code){
            $scientific_flag = $this->checkScientificNumber($code);
            
            if($scientific_flag){
                return sprintf('%0.0f',$code);
            }
            else {
                return $code;
            }
        }
        
        //public function get status date{}
        
        public function getStatusDate($op_id,$status_id){
            
            $status_date = OpStatus::model()->findByAttributes(
                    array(
                        'status_id' => $status_id,
                        'op_id' => $op_id
                    )
                    )->date;
            return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($status_date, 'yyyy-MM-dd'),'medium',null);
            
        }
        
        public function getOrderDetailLink($order_id){
            return CHtml::link($order_id,array('orders/orderdetail',array('order_id'=>$order_id)));            
        }
        
        public function checkPOExist($po_id){
           $order_product = self::model()->findByAttributes(array('po_id'=>$po_id));
           if(empty($order_product))
               return false;
           return true;
       }
       
       public function getDataForAllocateOrders(){
           $sql = sprintf(
                   'SELECT op.op_id, op.order_id, om.merchant_id, om.merchant_sku 
                       FROM order_products op 
                       JOIN op_merchants om ON om.op_id = op.op_id 
                       '
                   );
       }
       
       public function __cronAllocateOrder($order){
           $purchase = Purchase::model()->checkPurchaseByMerchant($order->merchant->merchant_id, $order->merchant->merchant_sku);
                   
            //purchase exits , unallocated and status_id < 25 and condition = NEW
            if(!empty($purchase) && $purchase->inv_flag == 1 && $purchase->status_id < 25 && $purchase->condition == CONDITION_NEW){

                $op_data = array();
                $op_data['status_id'] = $purchase->status_id;
                $op_data['po_id'] = $purchase->po_id;

                OrderProducts::model()->updateOrderProduct($order->op_id,$op_data);

                if(isset($order->warehouse->address->country_code) && isset($purchase->warehouse->address->country_code) && $purchase->warehouse->address->country_code == $order->warehouse->address->country_code){
                    unset($op_data);
                    $op_data = array();
                    $op_data['dest_wh_id'] = isset($order->order->client->company->import_wh_id)?$order->order->client->company->import_wh_id:0;
                    // update op dest_wh_id = wh_id of purchase
                    OrderProducts::model()->updateOrderProduct($order->op_id,$op_data); 
                    $response['success'][] = $order->op_id;
                }
                else {
                    unset($op_data);
                    $op_data = array();
                    $op_data['dest_wh_id'] = $purchase->wh_id;
                    // update op dest_wh_id = wh_id of purchase
                    OrderProducts::model()->updateOrderProduct($order->op_id,$op_data); 
                    $response['success'][] = $order->op_id;
                }
                
                Purchase::model()->resetInvFlag($purchase->po_id);
                //check po exits in order products yes  change statuse to merchant shipped                                
                if(OrderProducts::model()->checkPOExist($purchase->po_id)){
                    $order_data = array();
                    $order_data['status_id'] = $purchase->status_id; 
                    
                    OrderProducts::model()->updateOrderProductByPO($purchase->po_id,$order_data);
                }
            }
            else {
                $response['failed'][] = $order->op_id;
            }
       }
       
       public function cronAllocateOrders(){
           $criteria = new CDbCriteria();
           $criteria->select = 't.op_id, t.order_id, om.merchant_id, om.merchant_sku, t.dest_wh_id';
           $criteria->join = ' JOIN op_merchants as om ON om.op_id = t.op_id ';
           $criteria->condition = sprintf(' t.status_id = %d ',ST_NEW);
           
           
           $unallowcated_orders = self::model()->findAll($criteria);
           $response = array();
           $response['success'] = null;
           $response['failed'] = null;
           if(!empty($unallowcated_orders)){
               foreach($unallowcated_orders as $order){
                   $response[] = $this->__cronAllocateOrder($order);
               }
               
           }
           return false;
           
       }
       
       public function allocateOrders($merchant_id){
           $criteria = new CDbCriteria();
           $criteria->select = 't.op_id, t.order_id, om.merchant_id, om.merchant_sku, t.dest_wh_id';
           $criteria->join = ' JOIN op_merchants as om ON om.op_id = t.op_id ';
           $criteria->condition = sprintf(' t.status_id = %d AND om.merchant_id = %d ',ST_NEW,$merchant_id);           
           
           $unallowcated_orders = self::model()->findAll($criteria);
           $response = array();
           $response['success'] = null;
           $response['failed'] = null;
           if(!empty($unallowcated_orders)){
               foreach($unallowcated_orders as $order){
                   $response[] = $this->__cronAllocateOrder($order);
               }
               
           }
           return false;
       }
        
       public function getAllAmazonStatusNew(){
           //clear amazon tables
           Yii::app()->db->createCommand()->delete('amazon_cart','1');
           Yii::app()->db->createCommand()->delete('amazon_cart_order_product','1');
           //reset status 2 to 1 
           Yii::app()->db
                ->createCommand("UPDATE order_products SET status_id = 1 WHERE status_id=:status_id ")
                ->bindValues(array(':status_id' => PURCHASED))
                ->execute();
           
           $limit = 40;
           $tQuery = sprintf("SELECT count(*) as total FROM order_products as op JOIN op_merchants om ON om.op_id = op.op_id WHERE op.status_id = 1 and  om.merchant_id = 1 ");
           $tproduct = Yii::app()->db->createCommand($tQuery)->queryRow();
           
           if($tproduct['total'] > 0){
               $torders = $tproduct['total'];
               $total =  ceil($torders/$limit);
               for($j=0;$j<$total;$j++){
                    $sqlQuery = sprintf("SELECT om.op_id, om.merchant_sku, COUNT(*) as quantity FROM order_products as op JOIN op_merchants om ON om.op_id = op.op_id WHERE op.status_id = 1 and  om.merchant_id = 1 GROUP BY om.merchant_sku LIMIT %d, %d",($j * $limit),$limit);
                    
                    $amazonECS = Yii::app()->amazon;
                    $order_products = Yii::app()->db->createCommand($sqlQuery)->queryAll();
                    $asin_data = array();
                    $op_data = array();
                    if(!empty($order_products)){
                        foreach($order_products as $op){
                            
                            $op_data[] = $op['op_id'];
                            $asin_data[] = array(
                                    'ASIN' => $op['merchant_sku'],
                                    'Quantity' => $op['quantity']
                                );
                        }
                        $response = $amazonECS->responseGroup('Cart')->CartCreate($asin_data);
                        
                        if(!empty($response->Cart->CartItems)){
                            $cart_id = $response->Cart->CartId;
                            $hmac = $response->Cart->HMAC;
                            $purchase_url = $response->Cart->PurchaseURL;
                            $data = array();
                            $data['cart_id'] = $cart_id;
                            $data['hmac'] = $hmac;
                            $data['purchase_url'] = $purchase_url;
                            $data['sub_total'] = isset($response->Cart->SubTotal->Amount)?sprintf('%0.2f',($response->Cart->SubTotal->Amount/100)):0.00;
                            $data['error_info'] = isset($response->Cart->Errors)?serialize($response->Cart->Errors):'';
                            $data['date'] = new CDbExpression('Now()');
                            
                            AmazonCart::model()->addAmazonCart($data);
                            $flag = true;
                            foreach($op_data as $op){
                                $data = array();
                                $data['cart_id'] = $cart_id;
                                $data['op_id'] = $op;
                                
                                $response = AmazonCartOrderProduct::model()->addAmazonCartOrderProduct($data);  
                            }
                        }
                        if($torders == 0)
                            break;
                    }
               }
               
           }
       }
       
       
}
