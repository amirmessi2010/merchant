<?php

/**
 * This is the model class for table "op_merchants".
 *
 * The followings are the available columns in table 'op_merchants':
 * @property string $op_id
 * @property integer $merchant_id
 * @property string $merchant_sku
 * @property integer $category_id
 * @property string $cost
 */
class OpMerchants extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'op_merchants';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('op_id, merchant_id, merchant_sku, category_id, cost', 'required'),
			array('merchant_id, category_id', 'numerical', 'integerOnly'=>true),
			array('op_id', 'length', 'max'=>11),
			array('merchant_sku', 'length', 'max'=>50),
			array('cost', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('op_id, merchant_id, merchant_sku, category_id, cost', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'op_id' => 'Op',
			'merchant_id' => 'Merchant',
			'merchant_sku' => 'Merchant Sku',
			'category_id' => 'Category',
			'cost' => 'Cost',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('op_id',$this->op_id,true);
		$criteria->compare('merchant_id',$this->merchant_id);
		$criteria->compare('merchant_sku',$this->merchant_sku,true);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('cost',$this->cost,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OpMerchants the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function updateMerchants($data){
            $op_merchant = self::model()->findByPk($data['op_id']);     
            $op_merchant->merchant_id = (int)$data['merchant_id'];
            $op_merchant->merchant_sku = $data['merchant_sku'];
            $op_merchant->cost = $data['sales_price'];
            $op_merchant->category_id = isset($data['category_id'])?$data['category_id']:0;
            $op_merchant->verified_flag = isset($data['verified_flag'])?$data['verified_flag']:0;
            if($op_merchant->save()){
                return array(
                    'data' => $op_merchant->op_id,
                    'status' => true
                );
            }
        }
        
        public function resetMerchant($op_id){
            $data['op_id'] = $op_id;
            $data['merchant_id'] = 0;
            $data['merchant_sku'] = 0;
            $data['sales_price'] = 0;
            $data['category_id'] = 0;
            $data['verified_flag'] = 0;
            
            $res = $this->updateMerchants($data);
            return $res;
        }
}
