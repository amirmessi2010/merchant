<?php

/**
 * This is the model class for table "case_comments".
 *
 * The followings are the available columns in table 'case_comments':
 * @property integer $case_id
 * @property string $comment
 * @property integer $user_id
 * @property integer $interaction_type
 * @property string $create_date
 * @property integer $customer_rating
 */
class CaseComments extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'case_comments';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('case_id, comment, user_id, interaction_type, create_date, customer_rating', 'required'),
			array('case_id, user_id, interaction_type, customer_rating', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('case_id, comment, user_id, interaction_type, create_date, customer_rating', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'case_id' => 'Case',
			'comment' => 'Comment',
			'user_id' => 'User',
			'interaction_type' => 'Interaction Type',
			'create_date' => 'Create Date',
			'customer_rating' => 'Customer Rating',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('case_id',$this->case_id);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('interaction_type',$this->interaction_type);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('customer_rating',$this->customer_rating);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CaseComments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function addCaseComment($data){
            $case = new CaseComments;
            $case->setAttributes($data);
            $case->create_date = new CDbExpression('Now()');            
            
            if($case->save()){
                return array(
                    'data' => $case,
                    'status' => true
                );
            }
            else {
                return array(
                    'data' => false,
                    'errors' => $case->getErrors(),
                    'status' => false
                );
            }
            
        }
}
