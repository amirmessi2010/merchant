<?php

/**
 * This is the model class for table "purchase".
 *
 * The followings are the available columns in table 'purchase':
 * @property integer $po_id
 * @property integer $merchant_id
 * @property string $invoice_no
 * @property string $merchant_sku
 * @property string $description
 * @property integer $qty
 * @property string $unit_price
 * @property string $shipping_cost
 * @property string $condition
 * @property integer $inv_flag
 * @property string $box_id
 * @property integer $status_id
 * @property string $merchant_payment_id
 * @property string $remittance_id
 * @property integer $wh_id
 * @property string $create_date
 */
class Purchase extends CActiveRecord
{
    public $date_range;
    public $box_id;
    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'purchase';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('merchant_id, invoice_no, merchant_sku, description, qty, condition', 'required'),
			array('merchant_id,  inv_flag,qty, status_id, wh_id', 'numerical', 'integerOnly'=>true),
//                        array('invoice_no, merchant_sku','ECompositeUniqueValidator','message' => 'merchant_sku exists '),
                        array('box_id','filter','filter'=>array($this,'removeBrackets')),
			array('invoice_no, merchant_payment_id, remittance_id', 'length', 'max'=>50),
			array('merchant_sku', 'length', 'max'=>30),
			array('unit_price, shipping_cost', 'length', 'max'=>10),
			array('condition', 'length', 'max'=>100),
			array('box_id', 'length', 'max'=>60),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('po_id, merchant_id, invoice_no, merchant_sku, description, qty, unit_price, shipping_cost, condition, box_id, status_id, merchant_payment_id, remittance_id, wh_id,inv_flag', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'warehouse' => array(self::BELONGS_TO, 'Warehouse', 'wh_id'),
                    'tracking' => array(self::BELONGS_TO, 'Tracking', 'po_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'po_id' => 'Po',
			'merchant_id' => 'Merchant',
			'invoice_no' => 'Invoice No',
			'merchant_sku' => 'Merchant Sku',
			'description' => 'Description',
			'qty' => 'Qty',
			'unit_price' => 'Unit Price',
			'shipping_cost' => 'Shipping Cost',
			'condition' => 'Condition',
			'inv_flag' => 'Inv Flag',
			'box_id' => 'Box ID',
			'status_id' => 'Status',
			'merchant_payment_id' => 'Merchant Payment',
			'remittance_id' => 'Remittance',
			'wh_id' => 'Wh',
			'create_date' => 'Create Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($date_range)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('po_id',$this->po_id);
		$criteria->compare('merchant_id',$this->merchant_id);
		$criteria->compare('invoice_no',$this->invoice_no,true);
		$criteria->compare('merchant_sku',$this->merchant_sku,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('unit_price',$this->unit_price,true);
		$criteria->compare('shipping_cost',$this->shipping_cost,true);
		$criteria->compare('condition',$this->condition,true);
		$criteria->compare('box_id',$this->box_id,true);
		$criteria->compare('status_id',$this->status_id);
		$criteria->compare('merchant_payment_id',$this->merchant_payment_id,true);
		$criteria->compare('remittance_id',$this->remittance_id,true);
		$criteria->compare('wh_id',$this->wh_id);
                
                if (!empty($date_range)) {
                    $pos = strpos($date_range, '-');

                    if ($pos === false) {
                        $criteria->addCondition('t.create_date = :create_date', 'AND');
                        $criteria->params += array(':create_date' => date('Y-m-d', strtotime($date_range)));
                    } else {
                        $date_arr = explode('-', $date_range);

                        $criteria->addCondition('t.create_date >= :create_date1 AND t.create_date <= :create_date2', 'AND');
                        $criteria->params += array(':create_date1' => date('Y-m-d', strtotime($date_arr[0])), ':create_date2' => date('Y-m-d', strtotime($date_arr[1] . "+1 days")));
                    }
                }

                $criteria->order = 'create_date DESC ';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>array(
                            'pageSize'=>100,
                        ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Purchase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        /**
        * Model behaviors
        */
       public function behaviors() {
           return array(
               'dateRangeSearch' => array(
                   'class' => 'application.components.behaviors.EDateRangeSearchBehavior',
               ),
           );
       }
        
       //change the removeBrackets
        public function removeBrackets($code){
            return str_replace(array(')','('), null, $code);
        }
        
       public function checkPOExist($po_id){
           $purchase = self::model()->findByPk($po_id);
           if(empty($purchase))
               return false;
           return true;
       }
       
       public function checkPOStatusConfirm($po_id){
           $purchase = self::model()->findByAttributes(array('po_id'=>$po_id, 'status_id'=>CONFIRMED));
           if(empty($purchase))
               return false;
           return true;
       }
        public function addPurchase($data){
            $purchase = new Purchase();
            $data_array = array(
			'merchant_id' => $data['merchant_id'],
			'invoice_no' => $data['invoice_no'],
			'merchant_sku' => $data['merchant_sku'],
			'description' => $data['description'],
			'qty' => $data['qty'],
			'unit_price' => $data['unit_price'],
			'shipping_cost' => $data['shipping_cost'],
			'condition' => $data['condition'],
			'box_id' => isset($data['box_id'])?$data['box_id']:0,
			'status_id' => isset($data['status_id'])?$data['status_id']:0,
			'merchant_payment_id' => isset($data['merchant_payment_id'])?$data['merchant_payment_id']:0,
			'remittance_id' => isset($data['remittance_id'])?$data['remittance_id']:0,
			'wh_id' => isset($data['wh_id'])?$data['wh_id']:0,
                        'inv_flag' => isset($data['inv_flag'])?$data['inv_flag']:0,
		);
            
            $purchase->setAttributes($data_array);
            if($purchase->save()){
                return array(
                    'data' => $data['invoice_no'],
                    'po_id' => $purchase->po_id,
                    'status' => true
                );
            }
            else {                
                return array(
                    'data' => $data['invoice_no'],
                    'errors' => $purchase->getErrors(),
                    'po_id' => false,
                    'status' => false
                );
            }
        }
        
        
                
        public function getConfirmedOrders($format=null){
            $count=Yii::app()->db->createCommand('
                SELECT 
                    COUNT(*) 
                FROM purchase as p 
                WHERE status_id = 5')->queryScalar();
            if($format == 2){
                $select = ' invoice_no , merchant_sku as asin , box_id as tracking_number ';
            }
            else {
                $select = ' po_id , invoice_no , merchant_sku as asin , description as title , box_id as tracking_number ';
            }
            
            $sql= sprintf('
                SELECT 
                    %s 
                FROM purchase as p  
                WHERE status_id = %d',$select,CONFIRMED);
            return new CSqlDataProvider($sql, array(
                'totalItemCount'=>$count,                
                'pagination'=>array(
                    'pageSize'=>10000000,
                ),
            ));
            
        }
        
        public function updatePurchase($data){
                if(!empty($data)){
                    $purchase = self::model()->findByPk($data['po_id']);
                    $purchase->setAttributes($data);
                    
                    if($purchase->save()){
                        return true;
                    }
                    else {                
                        return false;
                    }
            }
        }
        
        public function checkPurchaseByMerchant($merchant_id, $merchant_sku){
            return self::model()->findByAttributes(array('merchant_id'=>$merchant_id, 'merchant_sku'=>$merchant_sku));
        }
        
        
        
        public function checkPurchasExistByMerchantSkuAndInvoiceNo($merchant_sku,$invoice_no,$qty){
            $criteria = new CDbCriteria();
            $criteria->compare('merchant_sku', $merchant_sku);
            $criteria->compare('invoice_no', $invoice_no,true);
            
            return self::model()->find($criteria);
        }
        
        public function resetInvFlag($po_id){
            $purchase = self::model()->findByPk($po_id);
            $purchase->inv_flag = 0; //reset inv flag
            $purchase->save();
        }
        
        
        public function updateBoxIds(){            
            
            $purchases = Yii::app()->db->createCommand('
                SELECT t.po_id, tr.master_box_id FROM purchase as t 
                JOIN tracking as tr ON t.po_id = tr.po_id 
                WHERE t.status_id < 25
            ')->queryAll();
            
            if(empty($purchases))
                return false;
            
            foreach($purchases as $purchase){
                $master_box_id = $purchase['master_box_id'];
                
                $p_data = array();
                if (Tracking::model()->checkIsMasterBoxOverOne($master_box_id)) {
                    //box id = MB<master_box_id>
                    $p_data['box_id'] = 'MB' . $master_box_id;
                } else {
                    //box id = UBN<po_id>)
                    $p_data['box_id'] = 'UBN' . trim($purchase['po_id']);
                }
                    $p_data['po_id'] = $purchase['po_id'];
                    
                    Purchase::model()->updatePurchase($p_data); // update box id in order product table
                    
                    
                    Tracking::model()->updateBoxIdByPo($purchase['po_id'],$p_data['box_id']);
            }
            
        }
        
        public function getPOId($data){
            $purchase = self::model()->findByAttributes(array('invoice_no'=>$data['invoice_no'],'merchant_sku'=>$data['asin']));
            if(empty($purchase))
                return false;
            return $purchase->po_id;
        }
        
        public function getPurchaseOrderId($data){
            $criteria = new CDbCriteria();
            $criteria->select = 't.po_id';
            $criteria->compare('invoice_no', $data['invoice_no'],true);
            $criteria->compare('merchant_sku',$data['asin']);
            
            $purchase = self::model()->find($criteria);
            
            if(empty($purchase))
                return false;
            
            return $purchase->po_id;
        }
        
        public function getPurchase($po_id){
            return self::model()->findByPk($po_id);
        }
}
