<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ValidationData extends CFormModel
{
	public $invoice_no;
        public $merchant_sku;
        public $description;
        public $qty;
        public $unit_price;
        public $shipping_cost;
        public $condition;
        //
        public $po_id;
        public $asin;
        public $title;
        public $tracking_number;
              
        /**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array('invoice_no,merchant_sku, description, qty, unit_price, shipping_cost, condition', 'required', 'on'=>'confirm_update','message'=>'required'),			
                        array('qty', 'numerical', 'integerOnly'=>true),
                        array('qty', 'qtyGrZero','on'=>'confirm_update'),
                        array('invoice_no', 'checkInvoicenoMerchantsku', 'on'=>'confirm_update'),
                        array('po_id,asin, title, tracking_number', 'required', 'on'=>'mark_order_1'),
                        array('invoice_no, asin,  tracking_number', 'required', 'on'=>'mark_order_2'),
                        array('tracking_number', 'checkTrackNumber','message'=>'invalid track ','on'=>'mark_order_1,mark_order_2'),
                        array('po_id','checkPOStatusConfirm','on'=>'mark_order_1'),
		);  
	}
        
        public function checkInvoicenoMerchantsku($attribute,$params)
        {
            $merchant_sku = isset($this->merchant_sku)?$this->merchant_sku:$this->asin;
            $response = Purchase::model()->checkPurchasExistByMerchantSkuAndInvoiceNo($merchant_sku,  $this->invoice_no,$this->qty);
            
            if(!empty($response)){
                $this->addError($attribute, 'exists');
            }
        }
        
        public function qtyGrZero($attribute,$params)
        {
            
            if($this->$attribute <= 0 )
              $this->addError($attribute, 'greater than 0 ');
        }
        
        public function checkTrackNumber($attribute,$params)
        {
            if($this->$attribute == '0')
              $this->addError($attribute, 'invalid track no ');
        }
        
        public function validateData($scenario, $data, &$errors){
            $model = new ValidationData($scenario);
            $model->attributes = $data;
            
            if($model->validate()){
                return true;
            }
            $errors = $model->getErrors();
            
            return false;
        }
        
        public function passwordStrength($attribute,$params)
        {
            if ($params['strength'] === self::WEAK)
                $pattern = '/^(?=.*[a-zA-Z0-9]).{5,}$/';  
            elseif ($params['strength'] === self::STRONG)
                $pattern = '/^(?=.*\d(?=.*\d))(?=.*[a-zA-Z](?=.*[a-zA-Z])).{5,}$/';  

            if(!preg_match($pattern, $this->$attribute))
              $this->addError($attribute, 'your password is not strong enough!');
        }
        
        public function checkPOStatusConfirm($attribute,$params){
           $purchase = Purchase::model()->findByAttributes(array('po_id'=>$this->$attribute, 'status_id'=>CONFIRMED));
           if(empty($purchase)){
               $this->addError($attribute, 'po id status not confirmed');
               return false;
           }
               
           return true;
       }
       
       public function checkMasterBoxIdZero($attribute,$params){
           $attribute = intval($this->$attribute);
           if(empty($attribute) || $attribute == '0' || $attribute == 0){
               $this->addError($attribute, 'empty/0');
               return false;
           }
           return true;
       }
       
       public function checkPOExists($attribute,$params){
           $tracking = Tracking::model()->findByPk(array('po_id'=>$this->$attribute));
           if(empty($tracking)){
               $this->addError($attribute, 'po id exits');
               return false;
           }
               
           return true;
       }
	
}