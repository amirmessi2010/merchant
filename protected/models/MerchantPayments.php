<?php

/**
 * This is the model class for table "merchant_payments".
 *
 * The followings are the available columns in table 'merchant_payments':
 * @property string $merchant_payment_id
 * @property integer $merchant_id
 * @property string $reference_no
 * @property string $amount
 * @property string $date
 * @property integer $mode
 */
class MerchantPayments extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'merchant_payments';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('merchant_id, reference_no, amount, date, mode', 'required'),
			array('merchant_id, mode', 'numerical', 'integerOnly'=>true),
			array('reference_no', 'length', 'max'=>80),
			array('amount', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('merchant_payment_id, merchant_id, reference_no, amount, date, mode', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'merchant_payment_id' => 'Merchant Payment',
			'merchant_id' => 'Merchant',
			'reference_no' => 'Reference No',
			'amount' => 'Amount',
			'date' => 'Date',
			'mode' => 'Mode',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('merchant_payment_id',$this->merchant_payment_id,true);
		$criteria->compare('merchant_id',$this->merchant_id);
		$criteria->compare('reference_no',$this->reference_no,true);
		$criteria->compare('amount',$this->amount,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('mode',$this->mode);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MerchantPayments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
