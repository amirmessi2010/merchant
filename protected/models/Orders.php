<?php

/**
 * This is the model class for table "orders".
 *
 * The followings are the available columns in table 'orders':
 * @property string $order_id
 * @property integer $client_id
 * @property string $client_order_no
 * @property string $address_id
 * @property string $discount_amount
 * @property integer $discount_type
 * @property string $create_date
 */
class Orders extends CActiveRecord
{
    public $client_name;
    public $order_date;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orders';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('client_id, client_order_no, address_id', 'required'),
                        array('client_order_no','filter','filter'=>array($this,'changeScientificNumber')),
                        array('create_date','default',
                                'value'=>new CDbExpression('NOW()'),
                                'setOnEmpty'=>false,'on'=>'insert'),
			array('client_id', 'numerical', 'integerOnly'=>true),
			array('client_order_no', 'length', 'max'=>30),
			array('address_id', 'length', 'max'=>11),
			array('discount_amount', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('order_id, client_id, client_order_no, address_id, discount_amount, discount_type, create_date', 'safe', 'on'=>'search'),
		);
	}

        public function checkScientificNumber($number){
            $str_arr = array('+','.');
            $str_con_flag = false;
            foreach($str_arr as $str){
                if(strpos($number, $str) !== false){
                    $str_con_flag = true;
                }
            }
            
            return $str_con_flag;
        }
        //change the scientificnumber to float
        public function changeScientificNumber($code){
            $scientific_flag = $this->checkScientificNumber($code);
            
            if($scientific_flag){
                return sprintf('%0.0f',$code);
            }
            else {
                return $code;
            }
        }
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'order_products' => array(self::HAS_MANY, 'OrderProducts', 'order_id'),
                    'address' => array(self::BELONGS_TO, 'Address', 'address_id'),
                    'client' => array(self::BELONGS_TO, 'Clients', 'client_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'order_id' => 'Order',
			'client_id' => 'Client',
			'client_order_no' => 'Client Order No',
			'address_id' => 'Address',
			'discount_amount' => 'Discount Amount',
			'discount_type' => 'Discount Type',
			'create_date' => 'Create Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('order_id',$this->order_id,true);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('client_order_no',$this->client_order_no,true);
		$criteria->compare('address_id',$this->address_id,true);
		$criteria->compare('discount_amount',$this->discount_amount,true);
		$criteria->compare('discount_type',$this->discount_type);
		$criteria->compare('create_date',$this->create_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Orders the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function checkOrder($data){
            
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
//                $criteria->condition = sprintf("client_id = %d AND client_order_no = '%s'",$data['client_id'],$data['']);
		$criteria->compare('client_id',isset($data['client_id'])?$data['client_id']:null);
		$criteria->compare('client_order_no',isset($data['client_order_no'])?$this->changeScientificNumber($data['client_order_no']):null);

		return self::model()->find($criteria);
        }
        
        public function addOrder($data){
            $order = new Orders;
            
            $order_data = array(
			'client_id' => isset($data['client_id'])?$data['client_id']:null,
			'client_order_no' => isset($data['client_order_no'])?$data['client_order_no']:null,
			'address_id' => isset($data['address_id'])?$data['address_id']:null,
			'discount_amount' => isset($data['discount_amount'])?$data['discount_amount']:null,
			'discount_type' => isset($data['discount_type'])?$data['discount_type']:null
		);
            $order->setAttributes($order_data);
            $order->discount_type = isset($data['discount_type'])?$data['discount_type']:'bucks';
            if($order->validate() && $order->save()){
                return array(
                    'status' => true,
                    'data' => $order
                );
            }
            else {
                return array(
                    'status' => false,
                    'data' => false,
                    'errors' => $order->getErrors()
                );
            }
        }
        /* Get all the companies orders
         * @return Orders collection model         *          * 
         */
        public function getOrders(){
            $criteria = new CDbCriteria();
            $criteria->select = '*';
            $criteria->join = ' 
                JOIN clients AS c ON t.client_id = c.client_id';
            $criteria->condition = sprintf(' c.company_id = %d',$company_id);
        }

        /* Get orders
         * @params integer $order_id 
         * @params integer $company_id 
         * @return Orders collection model         *          * 
         */
        public function getOrder($order_id,$company_id){
            $criteria = new CDbCriteria();
            $criteria->select = 't.*, c.name as client_name, t.create_date as order_date';
            $criteria->join = ' JOIN `clients` AS `c` ON t.client_id = c.client_id';  
            $criteria->condition = sprintf(' c.company_id = %d AND t.order_id = %u',$company_id,$order_id);          
            
            return self::model()->find($criteria);
        }
        
        /*
         * Get order products of order
         * @params object $order
         * @return order products collection model
         */
        public function getOrderProducts($order=null){
            if(!empty($order))
                return $order->order_products;
            return $this->order_products;
        }
        
        /*
         * Get address object of order
         * @params integer $address_id
         * @return address model
         */
        public function getAddress($address_id){
            return Address::model()->getAddress($address_id);
        }
        
        public function getMerchantShippedDate($op_id,$status_id = MERCHANT_SHIPPED){
            $criteria = new CDbCriteria();
            $criteria->select = 't.date';
            $criteria->condition = sprintf('t.op_id = %d AND t.status_id = %d',$op_id,$status_id);
            return OpStatus::model()->find($criteria);
        }
        

        /*
         */
        
        public function getOrderProductsHeader(){
            return array(
                'op id',
                'c2i sku',
                'merchant sku',
                'merchant name',
                'description',
                'qty',
                'sales price',
                'merchant invoice no',
                'Actions',
            );
        }
        
        public function isScientificStr($str) {
            $str = trim($str);
            if (empty($str)) return FALSE;
            if (strpos($str, '.')) return TRUE;
            if (strpos($str,'+')) return TRUE;

            return FALSE;

        }
}
