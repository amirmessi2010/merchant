<?php

/**
 * This is the model class for table "cases".
 *
 * The followings are the available columns in table 'cases':
 * @property integer $case_id
 * @property string $order_id
 * @property string $cust_name
 * @property string $cust_phone
 * @property string $cust_email
 * @property string $owner
 * @property integer $group_id
 * @property string $type
 * @property integer $status
 * @property string $create_date
 * @property string $modified_date
 */
class Cases extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cases';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('order_id, cust_name, cust_phone, cust_email, owner, group_id, type, status, create_date', 'required'),
			array('group_id, status', 'numerical', 'integerOnly'=>true),
			array('order_id', 'length', 'max'=>11),
			array('cust_name', 'length', 'max'=>100),
			array('cust_phone', 'length', 'max'=>15),
			array('cust_email', 'length', 'max'=>110),
			array('owner', 'length', 'max'=>30),
			array('type', 'length', 'max'=>14),
			array('modified_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('case_id, order_id, cust_name, cust_phone, cust_email, owner, group_id, type, status, create_date, modified_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'case_id' => 'Case',
			'order_id' => 'Order',
			'cust_name' => 'Cust Name',
			'cust_phone' => 'Cust Phone',
			'cust_email' => 'Cust Email',
			'owner' => 'Owner',
			'group_id' => 'Group',
			'type' => 'Type',
			'status' => 'Status',
			'create_date' => 'Create Date',
			'modified_date' => 'Modified Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('case_id',$this->case_id);
		$criteria->compare('order_id',$this->order_id,true);
		$criteria->compare('cust_name',$this->cust_name,true);
		$criteria->compare('cust_phone',$this->cust_phone,true);
		$criteria->compare('cust_email',$this->cust_email,true);
		$criteria->compare('owner',$this->owner,true);
		$criteria->compare('group_id',$this->group_id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('modified_date',$this->modified_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cases the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function addCase($data){
            $case = new Cases;
            $case->setAttributes($data);
            $case->create_date = new CDbExpression('Now()');
            $case->modified_date = new CDbExpression('Now()');
            
            if($case->save()){
                return array(
                    'data' => $case,
                    'status' => true
                );
            }
            else {
                return array(
                    'data' => false,
                    'errors' => $case->getErrors(),
                    'status' => false
                );
            }
            
        }
        
        public function creatCase($data){
            
            $response_c = $this->addCase($data);
            
            if(isset($response_c['data'])){
                $case = $response_c['data'];
                $data['case_id'] = $case->case_id;
                
                $repsonse_cc = CaseComments::model()->addCaseComment($data);
                
                if(isset($repsonse_cc['data'])){                    
                    return true;
                }
            }
            
            return false;
        }
}
