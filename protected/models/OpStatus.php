<?php

/**
 * This is the model class for table "op_status".
 *
 * The followings are the available columns in table 'op_status':
 * @property string $op_id
 * @property integer $status_id
 * @property string $date
 */
class OpStatus extends CActiveRecord
{
        public $max_status;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'op_status';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('op_id, status_id, date', 'required'),
			array('status_id', 'numerical', 'integerOnly'=>true),
			array('op_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('op_id, status_id, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'op_id' => 'Op',
			'status_id' => 'Status',
			'date' => 'Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('op_id',$this->op_id,true);
		$criteria->compare('status_id',$this->status_id);
		$criteria->compare('date',$this->date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OpStatus the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getOpStatus($op_id){
            return self::model()->findBySql(sprintf('SELECT MAX(status_id) as max_status FROM op_status WHERE op_id = %d',$op_id))->max_status;
        }
        
        public function deleteOpStatusByOpId($op_id){
            self::model()->deleteAllByAttributes(array('op_id'=>$op_id));
        }
}
