<?php

/**
 * This is the model class for table "address".
 *
 * The followings are the available columns in table 'address':
 * @property string $address_id
 * @property string $nickname
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 * @property string $mobile
 * @property string $street1
 * @property string $street2
 * @property string $city
 * @property string $state
 * @property string $country_code
 * @property string $zipcode
 * @property string $create_date
 */
class Address extends CActiveRecord
{
    public $full_name;
    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'address';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nickname, email, first_name, last_name, mobile, street1, street2, city, state, country_code, zipcode, create_date', 'required'),
			array('nickname, first_name, last_name', 'length', 'max'=>70),
			array('email', 'length', 'max'=>120),
			array('mobile', 'length', 'max'=>15),
			array('street1, street2', 'length', 'max'=>300),
			array('city, state', 'length', 'max'=>80),
			array('country_code', 'length', 'max'=>3),
			array('zipcode', 'length', 'max'=>8),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('address_id, nickname, email, first_name, last_name, mobile, street1, street2, city, state, country_code, zipcode, create_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'address_id' => 'Address',
			'nickname' => 'Nickname',
			'email' => 'Email',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'mobile' => 'Mobile',
			'street1' => 'Street1',
			'street2' => 'Street2',
			'city' => 'City',
			'state' => 'State',
			'country_code' => 'Country Code',
			'zipcode' => 'Zipcode',
			'create_date' => 'Create Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('address_id',$this->address_id,true);
		$criteria->compare('nickname',$this->nickname,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('mobile',$this->mobile,true);
		$criteria->compare('street1',$this->street1,true);
		$criteria->compare('street2',$this->street2,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('country_code',$this->country_code,true);
		$criteria->compare('zipcode',$this->zipcode,true);
		$criteria->compare('create_date',$this->create_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Address the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getFullName(){
            return $this->first_name. ' '. $this->last_name;
        }
}
