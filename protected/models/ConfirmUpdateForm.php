<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ConfirmUpdateForm extends CFormModel
{
	public $file_name;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array('file_name', 'required'),			
		);
	}
        
        public function mapReference(){
            return array(
                'invoice_no','merchant_sku','description','qty','unit_price','shipping_cost','condition'
            );
        }
        
        public function checkFormat($header){
            $map_reference = $this->mapReference();
            
            $diff_array = array_diff($map_reference, $header);
            
            if(empty($diff_array))
                return true; //the format is okie 
            return false;
        }
	
}