<?php

/**
 * This is the model class for table "tracking".
 *
 * The followings are the available columns in table 'tracking':
 * @property string $master_box_id
 * @property string $box_id
 * @property string $shipment_no
 * @property string $master_no
 * @property string $pouch_id
 * @property string $local_courier
 * @property string $lc_tracking_id
 * @property integer $po_id
 */
class Tracking extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tracking';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
//			array('master_box_id, box_id, shipment_no, master_no, pouch_id, local_courier, lc_tracking_id', 'required'),
                        array('master_box_id, po_id', 'required','on' => 'shiporder'),
                        array('master_box_id','filter','filter'=>array($this,'removeBrackets')),
			array('master_box_id', 'length', 'max'=>60),
                        array('po_id', 'numerical', 'integerOnly'=>true),
			array('box_id, shipment_no, master_no, pouch_id, local_courier, lc_tracking_id', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('master_box_id, box_id, po_id,  shipment_no, master_no, pouch_id, local_courier, lc_tracking_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'master_box_id' => 'Master Box',
			'box_id' => 'Box',
                        'po_id' => 'Po',
			'shipment_no' => 'Shipment No',
			'master_no' => 'Master No',
			'pouch_id' => 'Pouch',
			'local_courier' => 'Local Courier',
			'lc_tracking_id' => 'Lc Tracking',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('master_box_id',$this->master_box_id,true);
		$criteria->compare('box_id',$this->box_id,true);
		$criteria->compare('shipment_no',$this->shipment_no,true);
		$criteria->compare('master_no',$this->master_no,true);
		$criteria->compare('pouch_id',$this->pouch_id,true);
		$criteria->compare('local_courier',$this->local_courier,true);
		$criteria->compare('lc_tracking_id',$this->lc_tracking_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tracking the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function addTracking($data,$scenario=null){
            $tracking = new Tracking();
            
            if(!empty($scenario))
                $tracking->setScenario ($scenario);
            
            $tracking->setAttributes($data);
            
            if($tracking->save()){
                return array(
                    'data' => $tracking->po_id,                    
                    'status' => true
                );
            }
            else {                
                return array(                    
                    'errors' => $tracking->getErrors(),                    
                    'status' => false
                );
            }
        }
        
        public function checkIsMasterBoxExists($master_box_id){
            $purchase = self::model()->findByAttributes(array('master_box_id' => $master_box_id));
            
            if(empty($purchase))
                return false;
            return true;
        }
        
        public function checkIsMasterBoxOverOne($master_box_id){
           $purchase = self::model()->countByAttributes(array('master_box_id' => $master_box_id));
            
            if((int)$purchase <= 1)
                return true;
            return false;
        }
        
        public function updateBoxIdByPo($po_id,$box_id){
            $purchase = self::model()->findByAttributes(array('po_id'=>$po_id));
            $purchase->box_id = $box_id;           
            $purchase->save();
            
        }
        
        //change the removeBrackets
        public function removeBrackets($code){
            return str_replace(array(')','('), null, $code);
        }
}
