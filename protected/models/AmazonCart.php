<?php

/**
 * This is the model class for table "amazon_cart".
 *
 * The followings are the available columns in table 'amazon_cart':
 * @property string $cart_id
 * @property string $hmac
 * @property string $purchase_url
 * @property string $sub_total
 * @property integer $is_pressed
 * @property string $error_info
 * @property string $date
 */
class AmazonCart extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'amazon_cart';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cart_id, hmac, purchase_url, sub_total, date', 'required'),
			array('is_pressed', 'numerical', 'integerOnly'=>true),
			array('cart_id', 'length', 'max'=>80),
			array('hmac', 'length', 'max'=>35),
			array('sub_total', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('cart_id, hmac, purchase_url, sub_total, is_pressed, error_info, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cart_id' => 'Cart',
			'hmac' => 'Hmac',
			'purchase_url' => 'Purchase Url',
			'sub_total' => 'Sub Total',
			'is_pressed' => 'Is Pressed',
			'error_info' => 'Error Info',
			'date' => 'Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('cart_id',$this->cart_id,true);
		$criteria->compare('hmac',$this->hmac,true);
		$criteria->compare('purchase_url',$this->purchase_url,true);
		$criteria->compare('sub_total',$this->sub_total,true);
		$criteria->compare('is_pressed',$this->is_pressed);
		$criteria->compare('error_info',$this->error_info,true);
		$criteria->compare('date',$this->date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AmazonCart the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function addAmazonCart($data = array()){
            
            $model = new AmazonCart;
            $model->setAttributes($data);            
            if($model->save()){
                return $model;
            }
            else {                
                return false;
            }
        }
}
