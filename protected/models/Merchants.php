<?php

/**
 * This is the model class for table "merchants".
 *
 * The followings are the available columns in table 'merchants':
 * @property integer $merchant_id
 * @property integer $address_id
 * @property integer $company_id
 * @property integer $wh_id
 * @property integer $managed
 * @property integer $payment_terms
 */
class Merchants extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'merchants';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('merchant_id, address_id, company_id, wh_id, managed, payment_terms', 'required'),
			array('merchant_id, address_id, company_id, wh_id, managed, payment_terms', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('merchant_id, address_id, company_id, wh_id, managed, payment_terms', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'address' => array(self::BELONGS_TO, 'Address', 'address_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'merchant_id' => 'Merchant',
			'address_id' => 'Address',
			'company_id' => 'Company',
			'wh_id' => 'Wh',
			'managed' => 'Managed',
			'payment_terms' => 'Payment Terms',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('merchant_id',$this->merchant_id);
		$criteria->compare('address_id',$this->address_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('wh_id',$this->wh_id);
		$criteria->compare('managed',$this->managed);
		$criteria->compare('payment_terms',$this->payment_terms);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Merchants the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getMerchant($merchant_id){
            return self::model()->findByPk($merchant_id);
        }
}
