<?php
//php /var/www/test/cron.php maporder
class AllocateorderCommand extends CConsoleCommand {   
    
    public function run($arg){
        $response = OrderProducts::model()->cronAllocateOrders();
        if($response == false)
            die('No records found : No map orders ');
        
        printf("Success (%s)",  implode(',', $response['success']));
        printf("Failed (%s)",  implode(',', $response['failed']));
        return $response;
    }
}

?>

