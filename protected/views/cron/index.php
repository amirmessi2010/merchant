<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
$baseUrl = Yii::app()->theme->baseUrl; 
?>
<?php
$gridDataProvider = new CArrayDataProvider(array(
    array('id'=>1, 'firstName'=>'Mark', 'lastName'=>'Otto', 'language'=>'CSS','usage'=>'<span class="inlinebar">1,3,4,5,3,5</span>'),
    array('id'=>2, 'firstName'=>'Jacob', 'lastName'=>'Thornton', 'language'=>'JavaScript','usage'=>'<span class="inlinebar">1,3,16,5,12,5</span>'),
    array('id'=>3, 'firstName'=>'Stu', 'lastName'=>'Dent', 'language'=>'HTML','usage'=>'<span class="inlinebar">1,4,4,7,5,9,10</span>'),
	array('id'=>4, 'firstName'=>'Jacob', 'lastName'=>'Thornton', 'language'=>'JavaScript','usage'=>'<span class="inlinebar">1,3,16,5,12,5</span>'),
    array('id'=>5, 'firstName'=>'Stu', 'lastName'=>'Dent', 'language'=>'HTML','usage'=>'<span class="inlinebar">1,3,4,5,3,5</span>'),
));
?>
<div class="row-fluid">
	<div class="span6">
            <div class="table table-striped table-bordered table-condensed">
                <table class="items">
                    <thead>
                        <tr>
                            <th>Op Id</th><th>Status</th>
                        </tr>
                    </thead>
                    <?php if(isset($response['failed']) && !empty($response['failed'])){ ?>
                    <tbody>
                        <?php foreach($response['failed'] as $_reponse){ ?>
                            <tr>
                                <td><?php echo $_reponse; ?></td>
                                <td><?php echo '<span class="badge badge-important pull-right">fail</span>'; ?></td>
                            </tr>
                        <?php }?>
                        
                    </tbody>
                    <?php } ?>
                    <?php if(isset($response['success']) && !empty($response['success'])){ ?>
                    <tbody>
                        <?php foreach($response['success'] as $_reponse){ ?>
                            <tr>
                                <td><?php echo $_reponse; ?></td>
                                <td><?php echo 'ok'; ?></td>
                            </tr>
                        <?php }?>
                        
                    </tbody>
                    <?php } ?>
                </table>
            </div>
	</div><!--/span-->
	
</div><!--/row-->




