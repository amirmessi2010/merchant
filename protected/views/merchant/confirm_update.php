<?php
/* @var $this ConfirmUpdateFormController */
/* @var $model ConfirmUpdateForm */
/* @var $form CActiveForm */
?>

<div class="form padding">
    <h2>Confirm Orders</h2>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'confirm-update-form-confirm_update-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
                    <?php echo CHtml::label('Choose a file to upload','upload'); ?>
                    <?php
                    $this->widget('ext.EAjaxUpload.EAjaxUpload', array(
                        'id' => 'uploadFile',
                        'config' => array(
                            'action' => Yii::app()->createUrl($this->upload_action),
                            'allowedExtensions' => $this->upload_allowed_types, //array("jpg","jpeg","gif","exe","mov" and etc...
                            'sizeLimit' => $this->upload_maxlimit, // maximum file size in bytes
                            //  'minSizeLimit' => 10 * 1024 * 1024, // minimum file size in bytes
                            'onComplete' => "js:function(id, fileName, responseJSON){
                                $('#ConfirmUpdateForm_file_name').val(responseJSON.filename);
                                $('#upload_but').attr('disabled',false).removeClass('disabled');
                        }",
                        )
                    ));
                    ?>
                
		<?php echo $form->hiddenField($model,'file_name'); ?>
		<?php echo $form->error($model,'file_name'); ?>
                <?php  echo CHtml::link('Download Format',Yii::app()->getBaseUrl().'/uploads/templates/purchase_format.csv'); ?>
	</div>


	<div class="row buttons">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
                        'disabled' => true,
			'type'=>'primary',
			'label'=>'Upload',
                        'htmlOptions' => array(
                            'id' => 'upload_but',
                        )
		)); ?>
	</div>

<?php $this->endWidget(); ?>
<?php $summary->displaySummary(); ?>
<?php if(!empty($this->errors)) {
   echo '<ul>';
   echo '<li><strong>Row - &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Errors</strong></li>';
   foreach($this->errors as $row => $error){
       $e = array();
       $r = array();
       foreach($error as $key => $message){
           $r[$key] = $message;
       }
        $key = key($r);
        $message = Yii::app()->utility->recursiveImplode($r,' , ');
        printf('<li>%d -  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  [%s] = [%s]</li>',$row,$key ,$message);
   }
   echo '</ul>';
} ?>        
</div><!-- form -->

