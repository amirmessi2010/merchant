<?php
/* @var $this OrderProductsController */
/* @var $model OrderProducts */

$this->breadcrumbs=array(
	'Orders'=>array('index'),
	'Order Overview',
);


Yii::app()->clientScript->registerScript('search', "

$('.search-form form').submit(function(){
	$('#order-products-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
$('.status-change,.progress-cls').live('click',function(){
	var r = confirm('Are you sure');
        if (r == true)
          {
            $.getJSON($(this).attr('href'));
            $('#order-products-grid').yiiGridView('update');
          }
        
	return false;
});
$('.pay-cls').live('click',function(){
    var r = confirm('Are you sure');
        if (r == true)
          {
            $.getJSON($(this).attr('href'),function(data){window.open(data.url,'_blank');});
            $('#order-products-grid').yiiGridView('update');
          }        
	return false;      
});

");
?>

<h1>API Purchase</h1>

<div class="search-form" >
<?php $this->renderPartial('order_overview/_search',array(
	'model'=>$model, 'show_filter' => true
)); ?>
</div><!-- search-form -->
<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'order-products-grid',
	'dataProvider'=>$model->apiPurchase($status_id,$filter_by,$filter_value,$date_range),
        
	'columns'=>array(
                'op_id',
                'merchant.merchant_sku',
                'merchant.api_purchase.price',
                array(
                    'class'=>'CButtonColumn',                    
                    'template'=>'{pay}{pay_one}{done}',
                    'htmlOptions'=>array('style'=>'width:120px'),
                    'buttons'=>array(
                                  'pay' => array(
                                        'label'=>'<span class="btn btn-info">Pay</span>',
                                        'visible'=>'$data->status_id == 1 && isset($data->merchant->api_purchase->purchase_url) && $data->merchant->api_purchase->is_pressed == 0',
                                        'url'=>'($data->merchant->api_purchase->is_pressed == 0)?Yii::app()->createUrl("orders/setpressed", array("op_id"=>$data->op_id)):"#"',
                                        'options' => array('title'=>'Pay','target'=>'_blank','class'=>'pay-cls')
                                    ),  
                                    'pay_one' => array(
                                        'label'=>'<span class="btn">Revert</span>',
                                        'visible'=>'$data->status_id == 1 && isset($data->merchant->api_purchase->purchase_url) && $data->merchant->api_purchase->is_pressed == 1',
                                        'url'=>'Yii::app()->createUrl("orders/change", array("id"=>$data->op_id,"status_id"=>0,"model_name"=>"ApiPurchase"))',
                                        'options' => array('title'=>'Pay','target'=>'_blank','class'=>'progress-cls')
                                    ), 
                                    
                                  'done' => array(
                                        'label'=>'<span class="btn btn-warning">Done</span>',
                                        'visible'=>'$data->status_id == 1 && isset($data->merchant->api_purchase->purchase_url) ',
                                        'url'=>'Yii::app()->createUrl("orders/change", array("id"=>$data->op_id,"status_id"=>2))',
                                        'options'=>array(
                                                'class'=>'status-change','title'=>'Done' 
                                        ),
                                    ),
                                  )
            ),     
	),        
)); ?>

<?php // echo CHtml::ajaxSubmitButton('Yes',array('orderproducts/changeall','act'=>'doActive'), array('success'=>'reloadGrid')); ?>
<?php // echo CHtml::ajaxSubmitButton('No',array('menu/ajaxupdate','act'=>'doInactive'), array('success'=>'reloadGrid')); ?>
<script type="text/javascript">
$('#reportrange').daterangepicker(
    {
      ranges: {
         'Today': [moment(), moment()],
         'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
         'Last 7 Days': [moment().subtract('days', 6), moment()],
         'Last 30 Days': [moment().subtract('days', 29), moment()],
         'This Month': [moment().startOf('month'), moment().endOf('month')],
         'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
      },
      startDate: moment().subtract('days', 29),
      endDate: moment()
    },
    function(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('#date_range').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
);
</script>


