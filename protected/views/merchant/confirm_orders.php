<?php
/* @var $this OrderProductsController */
/* @var $model OrderProducts */

$this->breadcrumbs = array(
    'Merchant' => array('index'),
    'Confirm Orders',
);


Yii::app()->clientScript->registerScript('search', "

$('.search-form form').submit(function(){
	$('#purchase-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Confirmed Orders</h1>
<div class="search-form" >
    <?php
    $this->renderPartial('order_overview/_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->
<?php $form=$this->beginWidget('CActiveForm', array(
    'enableAjaxValidation'=>true,
)); ?>
 
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'purchase-grid',
    'dataProvider' => $model->search($date_range),
    'columns' => array(
        array(
            'id'=>'autoId',
            'class'=>'CCheckBoxColumn',
            'selectableRows' => '50',   
        ),
        'po_id','merchant_id','invoice_no','merchant_sku','description','qty','unit_price','shipping_cost',
        array('header' => 'Purchase Date',
            'value' => 'Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($data->create_date, "yyyy-MM-dd"),"medium",null)'),
    ),
));
?>
<script>
function reloadGrid(data) {
    $.fn.yiiGridView.update('purchase-grid');
    alert('action completed');
}
</script>
<?php echo CHtml::ajaxSubmitButton('Cancel Orders',array('merchant/cancelorders','act'=>'doActive'), array('success'=>'reloadGrid')); ?>
<?php $this->endWidget(); ?>
<script type="text/javascript">
    $('#reportrange').daterangepicker(
            {
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                    'Last 7 Days': [moment().subtract('days', 6), moment()],
                    'Last 30 Days': [moment().subtract('days', 29), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                },
                startDate: moment().subtract('days', 29),
                endDate: moment()
            },
    function(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('#date_range').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    );
</script>


