<?php
/* @var $this OrderProductsController */
/* @var $model OrderProducts */

$this->breadcrumbs = array(
    'Merchant' => array('index'),
    'New Orders',
);


Yii::app()->clientScript->registerScript('search', "

$('.search-form form').submit(function(){
	$('#order-products-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
$('.status-change').live('click',function(){
	var r = confirm('Are you sure');
        if (r == true)
          {
            $.getJSON($(this).attr('href'));
            $('#order-products-grid').yiiGridView('update');
          }
        
	return false;
});
");
?>

<h1>New Order</h1>
<div class="search-form" >
    <?php
    $this->renderPartial('order_overview/_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'order-products-grid',
    'dataProvider' => $model->overview($status_id, $merchant_id, $date_range,1),
    'columns' => array(
        array('header' => 'Order Date',
            'value' => 'Yii::app()->dateFormatter->formatDateTime($data->create_date,"medium",null)'),
        array('header' => 'Merchant SKU',
            'value' => '$data->merchant->merchant_sku'),
        array('header' => 'Title',
            'value' => 'substr($data->description,0,30)'),
        array('header' => 'Merchant Cost',
            'value' => '$data->merchant->cost'),
        array('header' => 'Qty',
            'value' => '$data->qty'),
        array(
            'class' => 'CButtonColumn',
            'template' => '{no}',
            'htmlOptions' => array('style' => 'width:65px'),
            'buttons' => array(                
                'no' => array(
                    'label' => 'Reject ',
                    'visible' => '$data->status_id == 1',
                    'url' => 'Yii::app()->createUrl("merchant/change", array("id"=>$data->op_id,"status_id"=>4))',
                    'options' => array(
                        'class' => 'status-change',
                    ),
                ),
            )
        ),
    ),
));
?>

<?php // echo CHtml::ajaxSubmitButton('Yes',array('orderproducts/changeall','act'=>'doActive'), array('success'=>'reloadGrid'));  ?>
<?php // echo CHtml::ajaxSubmitButton('No',array('menu/ajaxupdate','act'=>'doInactive'), array('success'=>'reloadGrid'));  ?>
<script type="text/javascript">
    $('#reportrange').daterangepicker(
            {
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                    'Last 7 Days': [moment().subtract('days', 6), moment()],
                    'Last 30 Days': [moment().subtract('days', 29), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                },
                startDate: moment().subtract('days', 29),
                endDate: moment()
            },
    function(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('#date_range').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    );
</script>


