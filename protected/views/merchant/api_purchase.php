<?php
$this->breadcrumbs=array(
	'Order Products'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List OrderProducts','url'=>array('index')),
	array('label'=>'Create OrderProducts','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('order-products-grid', {
		data: $(this).serialize()
	});
	return false;
});
$('.status-change').live('click',function(){
    var r = confirm('Are you sure');
        if (r == true)
          {
            $.getJSON($(this).attr('href'));
            $('#order-products-grid').yiiGridView('update');
          }        
	return false;      
});
");
?>

<h1>Manage Order Products</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'order-products-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'cart_id',
		'purchase_url',
		array(
                    'class'=>'CButtonColumn',                    
                    'template'=>'{link}{done}',
                    'htmlOptions'=>array('style'=>'width:120px'),
                    'buttons'=>array(
                                  'link' => array(
                                        'label'=>'<span class="btn btn-info">Link</span>',
                                        'url'=>'$data->purchase_url',
                                        'options' => array('title'=>'Link','target'=>'_blank','class'=>'pay-cls')
                                    ), 
                                  'done' => array(
                                        'label'=>'<span class="btn btn-warning">Done</span>',
                                        'url'=>'Yii::app()->createUrl("merchant/changepurchase", array("cart_id"=>$data->cart_id))',
                                        'options'=>array(
                                                'class'=>'status-change','title'=>'Done' 
                                        ),
                                    ),
                                  )
            ), 
	),
)); ?>
