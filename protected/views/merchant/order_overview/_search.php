<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
        
        <div id="reportrange" class="pull-right">
            <i class="fa fa-calendar fa-lg"></i>
            <span><?php echo date("F j, Y", strtotime('-30 day')); ?> - <?php echo date("F j, Y"); ?></span> <b class="caret"></b>
        </div>
	<?php echo $form->hiddenField($model,'date_range',array('class'=>'span5','id'=>'date_range')); ?>

	<div class="form-actions order-overview" >
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Go',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

<script>
function insertPurchaseInfo(filename){
    $.ajax({
        url : '<?php echo Yii::app()->getBaseUrl().'/merchant/insertpurchase'; ?>',
        data: {file_name: filename},  
        success: function(data){
            console.log(data);
        }
    });
}
</script>
