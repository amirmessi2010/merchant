<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
$baseUrl = Yii::app()->theme->baseUrl; 
?>
<?php //if ($summary->critical_error ) { ?>
<div class="row-fluid">
	<div class="span6">
            <div class="table table-striped table-bordered table-condensed">
                <table class="items">
                    <thead>
                        <tr>
                            <th>Type</th><th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                            <?php 
                            if ($summary->critical_error) {
                                printf('<tr><td>Critical Error</td><td>%s</td></tr>', $summary->error_msg);
                                return;
                            }
                            else {
                            ?>
                            <?php 
                            if(!empty($summary->file_type))
                                printf('<tr><td>File Type</td><td>%s</td></tr>', $summary->file_type);
                            if(!empty($summary->file_name))
                                printf('<tr><td>File Stored as</td><td>%s</td></tr>', $summary->file_name);  
                                printf('<tr><td>Total Records read</td><td>%s</td></tr>', $summary->records_read);
                                printf('<tr><td>Records uploaded</td><td>%s</td></tr>', $summary->records_uploaded); 
                                printf('<tr><td>Duplicates</td><td>%s</td></tr>', $summary->duplicates);
                                printf('<tr><td>Failed</td><td>%s</td></tr>', $summary->failed);
                             } ?>
                        
                    </tbody>
                </table>
            </div>
	</div><!--/span-->
	
</div><!--/row-->
<?php //} ?>




