<?php

class MerchantController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	/**
	 * @return array action filters
	 */
	public function filters() {
            return array(
                'accessControl', // perform access control for CRUD operations
            );
        }

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(			
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('neworders','change','insertpurchase','confirmupdate','confirmedorders','markshiporders','export','cancelorders','test'),
				'roles'=>array('merchant','amazon'),
			),
                        array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('apipurchase','changepurchase'),
				'roles'=>array('amazon'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
        public function actionNeworders(){
            
            $date_range = $status_id = $filter_by = $filter_value = null;
            $model = new OrderProducts('neworder');
            $model->unsetAttributes();
            $model->status_id = ST_NEW;
            $merchant_id = Yii::app()->user->getId();
            
            if(isset($_GET['OrderProducts'])){               
                $date_range = $_GET['OrderProducts']['date_range'];
                
            }
            $this->render('new_orders',array(
                    'model'=>$model,
                    'status_id' => ST_NEW,
                    'merchant_id' => $merchant_id,
                    'date_range' => $date_range
            ));
        }
        
        /* 
         * Perform the status  change 
         * @param int $id op id 
         * @param int $status_id op_id
         */
        public function actionChange($id=null,$status_id=null){  
            
            $order_product = OrderProducts::model()->findByPk($id);
            if(!empty($order_product) && $order_product->status_id == 1){
                $order_product->status_id = (int)$status_id;
                if($order_product->save()){
                    if($order_product->status_id == 4){                        
                        $repsonse = OpMerchants::model()->resetMerchant($order_product->op_id);

                        $case_data = array();
                        $case_data['order_id'] = $order_product->order_id;
                        $case_data['cust_name'] = $order_product->order->address->first_name.' '.$order_product->order->address->last_name;
                        $case_data['cust_phone'] = $order_product->order->address->mobile;
                        $case_data['cust_email'] = $order_product->order->address->email;
                        $case_data['group_id'] = 111;  //purchase team
                        $case_data['owner'] = Yii::app()->user->getId();

                        $case_data['status'] = CASE_CLOSED;

                        //for case comment 

                        $case_data['user_id'] = Yii::app()->user->getId();
                        $case_data['type'] = 'cancellation'; //outbound interaction_type
                        $case_data['interaction_type'] = 4;                        
                        $case_data['comment'] = sprintf('Order Cancelled by Merchant %s(%d)',Yii::app()->user->getState('logged_user')->username,Yii::app()->user->getId());
                        $case_data['customer_rating'] = 0;
                        $res = Cases::model()->creatCase($case_data);
                        echo CJSON::encode($repsonse);
                    } 
                }
                
            }
            
            Yii::app()->end();
        }
        
        
        public function actionInsertPurchase($file_name=null){
            
                $tmp_file_path = realpath($this->upload_path_tmp).DIRECTORY_SEPARATOR.$file_name;                
                
                $file_info = new SplFileInfo($tmp_file_path);
                $file_extension = $file_info->getExtension();

                $work_sheet = Yii::app()->excel->reader($tmp_file_path,$file_extension);
                $data = Yii::app()->excel->getRawData($work_sheet);                
                
                $merchant_id = Yii::app()->user->getId();
                $merchant = Merchants::model()->getMerchant($merchant_id);
                
                $response = array();
                foreach($data as $_data){
                    $_data['wh_id'] = $merchant->wh_id;
                    $_data['merchant_id'] = $merchant_id;
                    if(isset($_data['condition']) && empty($_data['condition'])){
                        $_data['condition'] = CONDITION_NEW;
                    }
                    for($i=1;$i<=(int)$_data['qty'];$i++){
                        $_data['invoice_no'] = $_data['invoice_no'].'_'.$i;
                        $response[] = Purchase::model()->addPurchase($_data);
                    }
                    
                }
                $success = array();
                $failed = array();
                
                foreach($response as $_response){
                    if($_response['status'] == true)
                        $success[] = $_response['data'];
                    else 
                        $failed[] = $_response['data'];
                }
                
                return CJSON::encode( array(
                    'success' => implode(',', $success),
                    'failed' => implode(',', $failed)
                ));
        }
        
        public function actionConfirmUpdate()
        {
            $model=new ConfirmUpdateForm;

            // uncomment the following code to enable ajax-based validation
            /*
            if(isset($_POST['ajax']) && $_POST['ajax']==='confirm-update-form-confirm_update-form')
            {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
            */
            $response = array();
            $summary = Yii::app()->summary;
            if(isset($_POST['ConfirmUpdateForm']))
            {
                $model->attributes=$_POST['ConfirmUpdateForm'];
                if($model->validate())
                {
                    $file_name = $_POST['ConfirmUpdateForm']['file_name'];
                    $tmp_file_path = realpath($this->upload_path_tmp).DIRECTORY_SEPARATOR.$file_name;                
                
                    $file_info = new SplFileInfo($tmp_file_path);
                    $file_extension = $file_info->getExtension();

                    $work_sheet = Yii::app()->excel->reader($tmp_file_path,$file_extension);
                    $header = Yii::app()->excel->getHeader($work_sheet);
                    unlink($tmp_file_path);
                    if($model->checkFormat($header)){
                        $data = Yii::app()->excel->getRawData($work_sheet);                
                        
                        $merchant_id = Yii::app()->user->getId();
                        $merchant = Merchants::model()->getMerchant($merchant_id);
                        $row = 2;
                        
                        foreach($data as $_data){
                            
                            if($_data['qty'] == 0){
                                    $summary->incrementRecordsRead();
                                }
                                else {
                                    for($i=1;$i<=$_data['qty'];$i++){
                                        $summary->incrementRecordsRead();
                                    }
                                }
                            $error = null;
                            $validate_data = new ValidationData;
                            if(!$validate_data->validateData('confirm_update', $_data, $error)){
                                
                                $duplicate_data = false;
                                
                                if(isset($error['invoice_no'])){
                                    $duplicate_data = true;
                                    unset($error['invoice_no']);  //remove duplicate error
                                }
                                else {
                                    $this->errors[$row] = $error;
                                }                                
                                if($duplicate_data){
                                    if($_data['qty'] == 0)
                                        $summary->incrementDuplicates();
                                    else {
                                        for($i=1;$i<=$_data['qty'];$i++){
                                            $summary->incrementDuplicates();
                                        }
                                    }
                                    
                                    
                                }
                                    
                                else 
                                    $summary->incrementFailed();
                                
                                $row ++;
                                continue;
                            }
                            $_data['merchant_id'] = $merchant_id;
                            $_data['wh_id'] = $merchant->wh_id;
                            $_data['status_id'] = CONFIRMED;
                            $_data['create_date'] = new CDbExpression('Now()');
                            $_data['inv_flag'] = 1;   // means the not allocated to any order
                            $_data['unit_price'] = sprintf("%0.2f",$_data['unit_price']); 
                            if(isset($_data['condition']) && empty($_data['condition'])){
                                $_data['condition'] = CONDITION_NEW;
                            }
                            $invoice_no = $_data['invoice_no'];
                            if((int)$_data['qty'] > 1){
                                $qty = (int)$_data['qty'];
                                for($i=1;$i<=$qty;$i++){ 
                                    $_data['qty'] = 1;
                                    //$summary->incrementRecordsRead();
                                    $_data['invoice_no'] = $invoice_no.'_'.$i;
                                    $response = Purchase::model()->addPurchase($_data);
                                    if($response['status']){
                                        $summary->incrementRecordsUploaded();
                                    }
                                    else {
                                        $this->errors[$row] = $response['errors'];
                                        $summary->incrementFailed();
                                        $row ++;
                                        continue;
                                    }
                                }
                            }
                            else {
                                //$summary->incrementRecordsRead();
                                $response = Purchase::model()->addPurchase($_data);
                                if($response['status']){
                                    $summary->incrementRecordsUploaded();
                                }
                                else {                                     
                                    $this->errors[$row] = $response['errors'];
                                    $summary->incrementFailed();
                                    $row ++;
                                    continue;
                                }
                            }
                            $row++;
                        }
                        
                    }
                    else {
                        $summary->setErrorMsg('Invalid format.');
                    }
                }
            }
            $this->render('confirm_update',array('model'=>$model,'summary'=>$summary));
        }
        
        public function actionConfirmedOrders(){
            $date_range = null;
            $model = new Purchase('search');
            $model->unsetAttributes();
            $model->status_id = CONFIRMED;
            $model->merchant_id = Yii::app()->user->getId();            
            if(isset($_GET['Purchase'])){  
                if(isset($_GET['Purchase']['date_range'])){
                   $date_range = isset($_GET['Purchase']['date_range'])?$_GET['Purchase']['date_range']:null;  
                }  
                
            }
            $this->render('confirm_orders',array(
                    'model'=>$model,
                    'date_range' => $date_range,                    
            ));
        }
        
         public function actionMarkShipOrders()
         {
            $model=new CommonForm();

            // uncomment the following code to enable ajax-based validation
            /*
            if(isset($_POST['ajax']) && $_POST['ajax']==='confirm-update-form-confirm_update-form')
            {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
            */
            $response = array();
            $summary = Yii::app()->summary;
            
            if(isset($_POST['CommonForm']))
            {
                $model->attributes=$_POST['CommonForm'];
                
                if($model->validate())
                {
                    $type = null;
                    if(isset($_POST['CommonForm']['type'])){
                        $type = $_POST['CommonForm']['type'];
                    }
                    $file_name = $_POST['CommonForm']['file_name'];
                    $tmp_file_path = realpath($this->upload_path_tmp).DIRECTORY_SEPARATOR.$file_name;                
                
                    $file_info = new SplFileInfo($tmp_file_path);
                    $file_extension = $file_info->getExtension();

                    $work_sheet = Yii::app()->excel->reader($tmp_file_path,$file_extension);
                    $header = Yii::app()->excel->getHeader($work_sheet);
                    
                    if($type == 2) {
                        $map_format = array(
                            'invoice_no','asin','tracking_number'
                        );
                    }
                    else {
                        $map_format = array(
                            'po_id','invoice_no','asin','title','tracking_number'
                        );
                    }
                    
                    
                    $model->setMapReference($map_format);
                    if($model->checkFormat($header)){
                        $data = Yii::app()->excel->getRawData($work_sheet);   
                        $row = 2;
                        
                        foreach($data as $_data){
                            
                            $summary->incrementRecordsRead();
                            $error = null;
                            $validate_data = new ValidationData;
                            if(!$validate_data->validateData('mark_order'.'_'.$type, $_data, $error)){
                                if(array_key_exists('invoice_no', $error)){
                                    $summary->incrementDuplicates();
                                }
                                else {
                                    $this->errors[$row] = $error;
                                    $summary->incrementFailed();
                                }
                                
                                $row++;
                                continue;
                            }
                                                        
                            //for format 2
                            if($type == 2){
                                $_data['po_id'] = Purchase::model()->getPurchaseOrderId($_data);
                                if(!$_data['po_id']){
                                    $summary->incrementFailed();
                                    $summary->addToErrlog($_data['invoice_no'].' no purchase order.');
                                    $row++;
                                    continue;
                                }
                            }
                            
                            $purchase_data = array();
                            $po_id = (int)trim($_data['po_id']);
                            
                            //check if the status = 5 yes update to 10
                            if(Purchase::model()->checkPOExist($po_id) && Purchase::model()->checkPOStatusConfirm($po_id)){
                                $purchase_data['po_id'] = $po_id;  
                                $purchase_data['status_id'] = MERCHANT_SHIPPED;  
                                
                                //check po exits in order products yes  change statuse to merchant shipped                                
                                if(OrderProducts::model()->checkPOExist($po_id)){
                                    $order_data = array();
                                    $order_data['status_id'] = MERCHANT_SHIPPED; 
                                    OrderProducts::model()->updateOrderProductByPO($po_id,$order_data);
                                }
                                if(Purchase::model()->updatePurchase($purchase_data)){
                                    $master_box_id = trim($_data['tracking_number']);
                                    
                                    //insert into tracking table
                                    $tracking = array();
                                    $tracking['master_box_id'] = $master_box_id;
                                    $tracking['po_id'] = $po_id;
                                    /*if(Tracking::model()->checkIsMasterBoxOverOne($master_box_id)){
                                        //box id = MB<master_box_id>
                                        $tracking['box_id'] = 'MB'.$master_box_id;
                                    }
                                    else {
                                        //box id = UBN<po_id>)
                                        $tracking['box_id'] = 'UBN'.trim($po_id);
                                    }
                                    
                                    $purchase_data['box_id'] = $tracking['box_id'];
                                    Purchase::model()->updatePurchase($purchase_data); // update box id in order product table*/
                                    
                                    if($response = Tracking::model()->addTracking($tracking,'shiporder')){
                                        
                                        if($response['status']){
                                            $summary->incrementRecordsUploaded();
                                        }
                                        else {
                                            printf($_data['invoice_no'].' track update failed');
                                            $summary->addToErrlog($_data['invoice_no'].' track update failed');
                                            $summary->incrementFailed();
                                        }
                                    }
                                }
                                else {    
                                    printf($_data['invoice_no'].' purchase update failed');
                                    $summary->addToErrlog($_data['invoice_no'].' purchase update failed');
                                    $summary->incrementFailed();
                                }
                                
                            }
                            else {
                                $summary->incrementDuplicates();
                            }
                            $row++;
                        }
                        //allocate order                        
//                        OrderProducts::model()->cronAllocateOrders();
                        
                        //update box_id
                        Purchase::model()->updateBoxIds();
                    }
                    else {
                        $summary->setErrorMsg('Invalid format.');
                    }
                }
            }
            $this->render('common_form',array('model'=>$model,'summary'=>$summary,'title'=>'Ship confirm orders','formsetting'=>array('type' => true)));
        }
        
        
        //cancel order from merchant 
        public function actionCancelOrders(){
            $po_ids = $_POST['autoId'];
            
            if(count($po_ids)>0)
            {
                foreach($po_ids as $po_id)
                {
                    
                    if(OrderProducts::model()->checkPOExist($po_id)){
                        $order_product = OrderProducts::model()->getOrderProductPO($po_id);
                        $op_id = $order_product->op_id;
                        //check op status
                        $status_id = OpStatus::model()->getOpStatus($op_id);
                        
                        $purchase_data = array();
                        $purchase_data['po_id'] = $po_id;
                        
                        if(in_array($status_id, array(RIP,RETURN_TO_MERCHANT,REJECTED,PRE_SHIP_CANCEL))){
                            
                            //change purchase (po_id) status to RETURNED
                            $purchase_data['status_id'] = RETURN_TO_MERCHANT;                            
                            $po_response = Purchase::model()->updatePurchase($purchase_data);
                            echo 'ok';

                        }
                        //for status 5,10,15
                        elseif(in_array($status_id, array(CONFIRMED,MERCHANT_SHIPPED,REGISTERED))){
                            
                            $purchase_data['status_id'] = PRE_SHIP_CANCEL; 
                            $po_response = Purchase::model()->updatePurchase($purchase_data);
                            
                            $purchase = Purchase::model()->getPurchase($po_id);
                            
                            $case_data = array();
                            $case_data['order_id'] = $order_product->order_id;
                            $case_data['cust_name'] = $order_product->order->address->first_name.' '.$order_product->order->address->last_name;
                            $case_data['cust_phone'] = $order_product->order->address->mobile;
                            $case_data['cust_email'] = $order_product->order->address->email;
                            $case_data['group_id'] = PURCHASE;  //purchase team
                            $case_data['owner'] = Yii::app()->user->getId();

                            $case_data['status'] = CASE_CLOSED;

                            //for case comment 

                            $case_data['user_id'] = Yii::app()->user->getId();
                            $case_data['type'] = 'cancellation'; //outbound interaction_type
                            $case_data['interaction_type'] = SYSTEM;                        
                            $case_data['comment'] = sprintf('Merchant Cancelled %d "%s" %d',$po_id,$purchase->description,$purchase->qty);
                            $case_data['customer_rating'] = 0;
                            $res = Cases::model()->creatCase($case_data);
                            
                            //reset the op merchant 
                            OpMerchants::model()->resetMerchant($op_id);
                            
                            //update the order product to new 
                            $op_data = array();
                            $op_data['status_id'] = ST_NEW;
                            OrderProducts::model()->updateOrderProduct($op_id, $op_data);
                            
                            echo 'okie';
                            
                        }
                        else {
                            //roll back the op_status to 1 
                           $purchase_data['status_id'] = ST_NEW;                            
                           $po_response = Purchase::model()->updatePurchase($purchase_data);
                           
                            //delete op_statuses 
                            OpStatus::model()->deleteOpStatusByOpId($op_id);
                            $op_data = array();
                            $op_data['status_id'] = ST_NEW; //change status to 1
                            $op_data['po_id'] = 0; // reset po_id
                            $op_response = OrderProducts::model()->updateOrderProduct($op_id,$op_data);
                            
                            
                            //reset the op merchants
                            OpMerchants::model()->resetMerchant($op_id);                            
                            
                            //add cases for order and comment 
                            $merchant_id = Yii::app()->user->getId();
                            $merchant = Merchants::model()->getMerchant($merchant_id);
                            
                            $case_data = array();
                            $case_data['order_id'] = $order_product->order_id;
                            $case_data['cust_name'] = $merchant->address->first_name.' '.$merchant->address->last_name;
                            $case_data['cust_phone'] = $merchant->address->mobile;
                            $case_data['cust_email'] = $merchant->address->email;
                            $case_data['owner'] = 1;
                            $case_data['group_id'] = UNKNOWN_GRP;
                            $case_data['type'] = 'cancellation';
                            $case_data['status'] = 1;
                            
                            $case_repsonse = Cases::model()->addCase($case_data);
                            
                            $purchase = Purchase::model()->getPurchase($po_id);
                            
                            $description = isset($purchase->description)?$purchase->description:'';
                            $qty = isset($purchase->qty)?$purchase->qty:'';
                            
                            $case_comment = array();
                            $case_id = $case_repsonse['data']->case_id;
                            $case_comment['case_id'] = $case_id;
                            $case_comment['comment'] = sprintf(' Merchant Cancelled purchase no: %d Description:"%s"  Qty: %d',$po_id,$description,$qty);
                            $case_comment['user_id'] = 0;
                            $case_comment['interaction_type'] = 4;                            
                            $case_comment['customer_rating'] = 0;
                            $cc_response = CaseComments::model()->addCaseComment($case_comment);
                            
                            if($cc_response['status'])
                                echo 'ok';
                            
                        }
                    }
                    else {
                        echo 'Failed : No PO ID';
                    }
                }
            }
            else {
                        echo 'Faile: No Data Selected';
                    }
        }

        public function actionExport($type = null,$format=null){
            $export_data = array();
            switch($type){
                case 'mark_ship_order':
                    if($format == 2){
                        $export_data[1] = array('invoice_no','asin','tracking_number');
                    }
                    else {
                        $export_data[1] = array('po_id','invoice_no','asin','title','tracking_number');
                    }
                    
                    $data = Purchase::model()->getConfirmedOrders($format);                    
                    break;
            }
            
            $export_data = array_merge($export_data, $data->getData());
            
            Yii::app()->excel->download($type."_".date('Y_m_d'),'Unmapped Orders Record',$export_data);
        }
        
        public function actionTest(){
            $master_box_id = 'masterboxid1';
            echo $test = Tracking::model()->checkIsMasterBoxOverOne($master_box_id);
        }
        
        public function actionApiPurchase(){
            $model = new AmazonCart('search');
            
            $this->render('api_purchase',array('model'=>$model));
        }
        
        public function actionChangepurchase(){
            $id = $_GET['cart_id'];
            $cart_id = urldecode($id);
            $amz_products = AmazonCartOrderProduct::model()->findAllByAttributes(array('cart_id'=>$cart_id));
            
            if(!empty($amz_products)){
                
                $op_ids = array();
                foreach($amz_products as $pro){
                    $data = array();
                    $data['status_id'] = 2;
                    $op_id = $pro->op_id;
                    OrderProducts::model()->updateOrderProduct($op_id,$data);
                }
                    
                AmazonCart::model()->deleteAllByAttributes(array('cart_id'=>$cart_id));
                AmazonCartOrderProduct::model()->deleteAllByAttributes(array('cart_id'=>$cart_id));
            }
        }
}