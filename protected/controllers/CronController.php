<?php

class CronController extends Controller
{
        public $layout='//layouts/column1';

        public $error_page_message = 'No order found';
        /**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(			
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('allocateorders'),
				'roles'=>array('merchant'),
			),
                        array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('apipurchase'),
				'roles'=>array('amazon'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	public function actionAllocateOrders()
	{
            //allocate order
            $merchant_id = Yii::app()->user->getId();
            $response = OrderProducts::model()->allocateOrders((int)$merchant_id);
            $this->render('index',array('response'=>$response));
	}
        
        public function actionApipurchase(){
            OrderProducts::model()->getAllAmazonStatusNew();
            $response = array(
                'failed' => null,
                'success' => null,
            );
            $this->render('amz',array('response'=>$response));
        }

	
}