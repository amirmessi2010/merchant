<?php
/**
 * Paypal.php
 *
 * https://github.com/stdevteam/yii-paypal
 *
 * @author STDev <yii@st-dev.com>
 * @copyright 2013 STDev http://st-dev.com
 * @license released under dual license BSD License and LGP License
 * @package PayPal
 * @version 1.0
 */
class Text extends CComponent{
    
    public function init(){
        
    }
    public function generateKey($length = 60) {
        $chars = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
        shuffle($chars);
        return implode(array_slice($chars, 0, $length));
    }
    
    public function wbr($word, $length = 5) {
        return (strlen($word) > $length)?implode('</br>', str_split($word, $length)):$word;
    }
}