<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Excel extends CComponent{ 
    
    public $workSheet;
    public $header;
    public $data;
   
    public function init(){
        Yii::import('application.vendors.phpexcel.PHPExcel', true);
    }
    
    public function getHighestColumnWithData($objWorksheet){
        
        $col = 1;
        do{
            $data = $objWorksheet->getCellByColumnAndRow($i, 1)->getValue();
            if(empty($data))
                break;
            $col++;
        }while(true);
        
        return $col;
    }
    
    public function getHeader($objWorksheet = null){
        if(empty($objWorksheet))
            $objWorksheet = $this->workSheet;        
        
        $highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
        
        $header = array();
        for($col = 0; $col< $highestColumnIndex; $col++){
            $header[] =  $objWorksheet->getCellByColumnAndRow($col, 1)->getValue();
        }
        
        array_filter($header);
        $this->header = $header;
        return $header;
       
    }
    
    public function getTemplateArray($objWorksheet){
        if(empty($objWorksheet))
            $objWorksheet = $this->workSheet;        
        
        $highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
        
        $tmp_arr = array();
        for($col = 0; $col< $highestColumnIndex; $col++){
            $tmp_arr[$objWorksheet->getCellByColumnAndRow($col, 2)->getValue()] =  $objWorksheet->getCellByColumnAndRow($col, 1)->getValue();
        }
        
        array_filter($tmp_arr);
        
        return $tmp_arr;
    }
    
    public function getTemplateString($objWorksheet){
        if(empty($objWorksheet))
            $objWorksheet = $this->workSheet;        
        
        $highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
        
        $tmp_arr = array();
        for($col = 0; $col< $highestColumnIndex; $col++){
            $tmp_arr[] =  $objWorksheet->getCellByColumnAndRow($col, 2)->getValue();
        }
        $tmp_string = implode('-', $tmp_arr);        
        return trim($tmp_string);
    }

    public function getData($objWorksheet = null,$mapped_header=true){
        if(empty($objWorksheet))
            $objWorksheet = $this->workSheet;
        
        $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
        $highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
        $header = $this->getHeader($objWorksheet);
        $data = array();
        
        for($row=2;$row <= $highestRow; $row++){
            $tmp_header = $header;
            
            for($col = 0; $col<= $highestColumnIndex; $col++){
               $index = array_shift($tmp_header);
                if( isset($mapped_header[trim($index)]))
                    $data[$row][$mapped_header[trim($index)]] =  $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
            }

            // array_filter($data[$row]);
        }
        
        $this->data = $data;
        
        return $data;
    }
    
    public function getRawData($objWorksheet = null,$mapped_header=true){
        if(empty($objWorksheet))
            $objWorksheet = $this->workSheet;
        
        $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
        $highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
        $header = $this->getHeader($objWorksheet);
        $data = array();
        
        for($row=2;$row <= $highestRow; $row++){
            $tmp_header = $header;
            
            for($col = 0; $col<= $highestColumnIndex; $col++){
               $index = array_shift($tmp_header);
                if( !empty($index) )
                    $data[$row][$index] =  $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
            }

            // array_filter($data[$row]);
        }
        
        $this->data = $data;
        
        return $data;
    }
    
    public function reader($file_location,$file_type){
        
        $objReader = new PHPExcel();        
        switch ($file_type){
            case 'xlsx':
                $ip_type = 'Excel2007';
                break;
            case 'csv':
                $ip_type = 'CSV';
                break;
            default :
                $ip_type = 'Excel5';
                break;
        }
        
        $objReader = PHPExcel_IOFactory::createReader($ip_type);
        $objPHPExcel = $objReader->load(@$file_location);
        $objWorksheet = $objPHPExcel->getActiveSheet();
        $this->workSheet = $objWorksheet;
        return $objWorksheet;        
    }
    
    public function writer1($file_name,$title,$data){
        
        Yii::import('application.extensions.phpexcel.JPhpExcel');
        $xls = new JPhpExcel('UTF-8', false, $title);
        $xls->addArray($data);
        $xls->generateXML($file_name);
    }
    
    public function download($file_name,$title,$data) {
        $objPHPExcel = new PHPExcel();

        // Set properties        
        $objPHPExcel->getProperties()->setTitle($title);
        $objPHPExcel->getProperties()->setSubject($title);
        $objPHPExcel->getProperties()->setDescription($title);


        // Add some data       
        $objPHPExcel->setActiveSheetIndex(0);
        
        //write header
        
        $row = 1;
        foreach($data as $_data){ 
            $col_count = count($_data);
            
            for($col=0;$col<$col_count;$col++){
                
                //echo $_data[$col];
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, array_shift($_data));
            }
            $row ++;
        }


    // Save Excel 2007 file
//        echo date('H:i:s') . " Write to Excel2007 format\n";
//        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
//        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
//        header('Content-Disposition: attachment;filename="unmapped_order.xlsx"');
//        header('Cache-Control: max-age=0');
//        $objWriter->save('php://output');
        // Redirect output to a client’s web browser (Excel5)
        $file_name = $file_name.'.xls';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$file_name.'"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }
}
?>
