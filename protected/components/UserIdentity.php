<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    const ERROR_USERNAME_INVALID   = 3;
    const ERROR_STATUS_INVALID  = 4;
    public $username;
    public $password;
    
    function __construct($username, $password) {
        $this->username    = $username;
        $this->password = $password;
    }

    private $_id;
    public function authenticate()
    {
        $record=  MerchantLogin::model()->findByAttributes(array('username'=>$this->username));

        if($record===null)
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        else if($record->password !== md5($this->password))
            $this->errorCode=self::ERROR_PASSWORD_INVALID;        
        else
        {
            $this->_id=$record->merchant_id;
            $this->setState('logged_user', $record);
            $this->setState('roles', $record->roles);
            $this->errorCode=self::ERROR_NONE;
            return !$this->errorCode;
        }
        return $this->errorCode;
    }

    public function getId()
    {
        return $this->_id;
    }

}