<?php
/**
 * Summary Class 
 *
 * @author 
 * @copyright 
 * @license released under dual license BSD License and LGP License
 * @package Summary
 * @version 1.0
 */
class Summary extends CComponent{
    var $file_name = '';
    var $file_type = '';
    var $records_read = 0;
    var $duplicates = 0;
    var $records_uploaded = 0;
    var $failed = 0;
    var $error_msg = '';
    var $critical_error = false;
    var $errlog;
    var $tracking_arr = array();


    public function init(){
        
    }
    public function addToErrlog($errmsg){
        $this->errlog[] = $errmsg;
    }
    public function getRecordsRead(){
        return $this->records_read;
    }
    
    public function incrementRecordsRead(){
        $this->records_read++;
    }
      
    public function incrementRecordsUploaded(){
        $this->records_uploaded++;
    }
    
    public function incrementDuplicates(){
        $this->duplicates++;
    }
    
    public function incrementFailed(){
        $this->failed++;
    }

    public function setFileType( $file_type) {
        $this->file_type = $file_type;
    }

    public function setFileName( $file_name) {
        $this->file_name = $file_name;
    }

    public function setErrorMsg ( $message) {
        $this->critical_error = true;
        $this->error_msg = $message;
    }

    public function printErrLog($log = false) {
        if (!empty($this->errlog)) {
            printf('<u>Errors</u><br />');
            for($i = 0; $i < count($this->errlog); $i++ ) {
                printf('<b>%02d.</b> %s <br />', $i+1, $this->errlog[$i]);
//                if ($log)
//                    $log->log(sprintf("%02d. %s", $i+1, $this->errlog[$i]), PEAR_LOG_ERR);
            }
        }
    }


    public function displaySummary(){        
        Yii::app()->controller->renderPartial('application.views.common.summary', array('summary' => $this));
    }
    
    public function printSummary($log = false) {
        if ($this->critical_error) {
            printf("Critical Error : %s <br />", $this->error_msg);
            if ($log)
                $log->log('$this->error_msg', PEAR_LOG_ERR);
            return;
        }
        if(!empty($this->file_type))
            printf('File Type ...............[%s]<br />', $this->file_type);
        if(!empty($this->file_name))
            printf('File Stored as ..........[%s]<br />', $this->file_name);
        if(!empty($this->records_read))
            printf('Total Records read.......[%04d]<br />', $this->records_read);
        if(!empty($this->records_uploaded))
            printf('&nbsp;&nbsp;&nbsp Records uploaded.....[%04d]<br />', $this->records_uploaded);
        if(!empty($this->duplicates))
            printf('&nbsp;&nbsp;&nbsp Duplicates...........[%04d]<br />', $this->duplicates);
        if(!empty($this->failed))
            printf('&nbsp;&nbsp;&nbsp Failed...........[%04d]<br />', $this->failed);
        if ($log) {
            $log->log(sprintf('File Type ...............[%s]', $this->file_type), PEAR_LOG_NOTICE);
            $log->log(sprintf('File Stored as ..........[%s]', $this->file_name), PEAR_LOG_NOTICE);
            $log->log(sprintf('Total Records read.......[%04d]', $this->records_read), PEAR_LOG_NOTICE);
            $log->log(sprintf('&nbsp;&nbsp;&nbsp Records uploaded.....[%04d]', $this->records_uploaded), PEAR_LOG_NOTICE);
            $log->log(sprintf('&nbsp;&nbsp;&nbsp Duplicates...........[%04d]', $this->duplicates), PEAR_LOG_NOTICE);
        }
    }
    
    public function printSummaryXml($errors){
        $summary = array();
        $summary['file_type'] = $this->file_type;
        $summary['file_name'] = $this->file_name;
        $summary['records_read'] = $this->records_read;
        $summary['records_uploaded'] = $this->records_uploaded;
        $summary['duplicates'] = $this->duplicates;
        $summary['failed'] = $this->failed;
        $summary['errors'] = $errors;
        $xml = new SimpleXMLElement('<summary/>');
        array_walk_recursive($summary, array ($xml, 'addChild'));
        
        print $xml->asXML();
    }
}