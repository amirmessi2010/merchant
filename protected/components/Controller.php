<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
        public $pageTitle;
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
        
        public $upload_action = 'site/upload';
        public $upload_maxlimit = 10485760;
        public $upload_minlimit = 0;
        public $upload_allowed_types = array('xlsx','xls','csv');
        public $upload_path_tmp = 'uploads/tmp/';
        public $upload_path = 'uploads/';
        
        public $success;
        public $errors = array();  
        
        public $error_page_message = 'No page found';
        
        public $base_url ;
        
        public $page_size = 100;
        
        public function init(){
            $this->base_url = Yii::app()->getBaseUrl(true);
            $this->pageTitle = Yii::app()->params['pageTitle'];
                    
        }
        
        
}