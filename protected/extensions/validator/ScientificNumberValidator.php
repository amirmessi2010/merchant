<?php
/**
* ECompositeUniqueValidator class file.
*
* @author c@cba <c@cba-solutions.org>
*/

/**
* ECompositeUniqueValidator validates that the composite key defined by the attributes is unique in the corresponding database table.
*
* If the property 
*
* When using the {@link message} property to define a custom error message, the message
* may contain additional placeholders that will be replaced with the actual content.
* ECompositeUniqueValidator allows for the following placeholders to be specified:
* <ul>
* <li>{attributes}: replaced with a comma separated list of the labels of the given attributes.</li>
* <li>{values}: replaced with a comma separated list of the current values of the given attributes.</li>
* <li>{value_"attr"}: replaced with the value of the given attribute named "attr", e.g. use {value_name} to get the value of the attribute 'name'.</li>
* </ul>
*
* @author c@cba <c@cba-solutions.org>
* @version 1.0 (2013-12-25)
*/
class ScientificNumberValidator extends Validator
{
	/**
        * Validates the attribute of the object.
        * If there is any error, the error message is added to the object.
        * @param CModel $object the object being validated
        * @param string $attribute the attribute being validated
        */
       protected function validateAttribute($object,$attribute)
       {
           $pattern = '/[+\-]?(?:0|[1-9]\d*)(?:\.\d*)?(?:[eE][+\-]?\d+)?/';
           // extract the attribute value from it's model object
           $value=$object->$attribute;
           if(!preg_match($pattern, $value))
           {
               $this->addError($object,$attribute,'your password is too weak!');
           }
       }
        
        /**
         * Given active record class name returns new model instance.
         *
         * @param string $className active record class name.
         * @return CActiveRecord active record model instance.
         *
         * @since 1.1.14
         */
        protected function getModel($className)
        {
                return CActiveRecord::model($className);
        }
}