<?php
//*
//  Config params settings
//*/

$config['components'] = 
    array(
                'session' => array(
                    'timeout' => 99999999,
                 ),
                'excel'=>array(
                    'class'=>'application.components.Excel',
                ),
                'utility'=>array(
                    'class'=>'application.components.Utility',
                ),
                'summary'=>array(
                    'class'=>'application.components.Summary',
                ),
                'bootstrap'=>array(
                    'class'=>'bootstrap.components.Bootstrap',
                ),
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
        
                'amazon'=>array(
                    'class'=>'application.components.AmazonECS',
                    'accessKey' => AWS_API_KEY,
                    'secretKey' => AWS_API_SECRET_KEY,
                    'country' => 'US',
                    'associateTag' => AWS_ASSOCIATE_TAG,
                ),
		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
                        'showScriptName' => false,
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',                                                          
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',                                
                                array('api/orderinsert', 'pattern'=>'api/<type:\w+>', 'verb'=>'POST'), 
			),
		),
		/*
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),*/
		// uncomment the following to use a MySQL database
		
		'db'=>array(
			'connectionString' => sprintf('mysql:host=%s;dbname=%s',$db_host,$db_name),
			'emulatePrepare' => true,
			'username' => $db_user,
			'password' => $db_password,
//			'charset' => 'utf8',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				
				/*array(
					'class'=>'CWebLogRoute',
				),*/
				
			),
		),
                'user' => array(
                    'class' => 'WebUser',
                ),
	);
?>
