<?php
$config = array();
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
$db_name = 'omsindiatest';
$db_user = 'awsroot';
$db_password = '@w$rOOtDB123';
$db_host = 'localhost';
$db_port = '3306';

if (file_exists(dirname(dirname(dirname(__FILE__))) . '/dev-config.php')) {    
    require_once dirname(dirname(dirname(__FILE__))) . '/dev-config.php';
    $db_name = LOCAL_DB_NAME;
    $db_user = LOCAL_DB_USER;
    $db_password = LOCAL_DB_PASSWORD;
    $db_host = LOCAL_DB_HOST;
    $db_port = LOCAL_DB_PORT;
    
}
require_once 'params.php';
require_once 'components.php';
require_once 'modules.php';
Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'MERCHANT',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
                'application.vendors.phpexcel.PHPExcel',
                'ext.ECompositeUniqueValidator',
	),
	'theme'=>'abound',
	'modules'=>$config['modules'],

	// application components
	'components'=>$config['components'],

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>$config['params'],
);